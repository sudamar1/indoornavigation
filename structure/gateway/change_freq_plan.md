# CHANGE THE LORA FREQUENCY BAND

## GW
- global_config.json change => take a region configuration from pkt_fwd/install/configuration_files => insert it to /inavi-gateway/packet_forwarder/lora_pkt_fwd/ on the gw
- change region in chirpstack-network-server.toml
	- (there are many other regional parameters, i left them unchanged - refer to lora-alliance reginal params. doc) TODO: add the lora-alliance doc


## NODE
- lora_app.h - enable different band + uncomment different band in lorawan_conf.h (only one plan can be defined at a time!)
