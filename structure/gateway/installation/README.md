# Gateway installation guide

##	General information

The gateway is built on the reaspberry pi3 hardware and Raspbian LITE is used as the OS. The following guide describes a solution for LORAWAN gateway utilizing multiple open source projects. Following projects were used for the development:

* [lora_gateway](https://github.com/Lora-net/lora_gateway)
* [packet_forwarder](https://github.com/Lora-net/packet_forwarder)
* [RAK831-LoRaGateway-RPi](https://github.com/robertlie/RAK831-LoRaGateway-RPi)
* [chirpstack](https://www.chirpstack.io/)

Before the installation procedure please select the correct frequency plan based on the country.  
See the list of frequency plans based on country below:  
https://www.thethingsnetwork.org/docs/lorawan/frequencies-by-country.html  
The frequency plans can be found at:  
https://www.thethingsnetwork.org/docs/lorawan/frequency-plans.html  
If you know the frequency plan, look at the table and find the corresponding Region. It should match your location. This project uses the European region EU863-870. 

| Region | Frequency Plan |
|--------|----------------|
| AS1    | AS920-923      |
| **AS2**|**AS923-925**   |
| AU     | AU915-928      |
| CN     | CN470-510      |
| **EU** | **EU863-870**  |
| IN     | IN865-867      |
| KR     | KR920-923      |
| US     | US902-928      |

##    Step by step guide

###   Initial settings
**Step 1:** Download and install an image of debian [Raspbian LITE](https://www.raspberrypi.org/downloads/raspbian/)
* image should be a version before 20.8.2020 - otherwise gpio settings differ!
      - version released 20.8.2020 changes gpio settings and that causes pktfwd malfunction
      - concentrator can not be started - I believe SPI problem!  
      - alocate gpio pin 7 before SPI enable, will be alocated by kernel otherwise
      
      echo 7 > /sys/class/gpio/export

**Step 2:** Open 'raspi-config' enable SPI and SSH
* write country to wpa_supplicant.conf 

      sudo raspi-config  # network options => wifi => select country and then cancel

**Step 3:** Setup the Wi-fi connection (Add new network) => Two possible setups
      
      # -i represents interface
      wpa_cli -i wlan0
      # get current interface
      ifname
      # scan for available networks
      scan
      # list all registered networks in wpa_supplicant.conf
      list_networks
      # add network <number> is the id in list_networks
      add network <number>
      set_network ssid "<name>"
      set_network psk "<psk>"
      # connect the network with id <number>
      enable_network <number>
      
- use following to encrypt the psk `sudo wpa_passphrase "<network_name>" >> /etc/wpa_supplicant/wpa_supplicant.conf`

**OR**:
      
      sudo su
      wpa_passphrase "<network_name>" >> /etc/wpa_supplicant/wpa_supplicant.conf
      
- enter pswd when requested or if it waits for input 
- check configuration file (sudo nano wpa_supplicant.conf, exit nano: ctrl+x Y Enter)
* configure interface (Create WiFi connection to predefined network)
      
      wpa_cli -i wlan0 reconfigure

* check connection
      
      ifconfig wlan0 | grep inet
      
- if not connected do next bullet
* check configuration file

      sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
 
- "country=CZ" should be above the network in the conf file
- check connection
- finally if still not connected reboot Raspi 
      - wifi should be assigned, run: check connection bullet to check

* NETWORK ADMINISTRATION CONFIGURATION:

      sudo iwlist wlan0 scan  # to find all networks
      sudo nano /etc/wpa_supplicant/wpa_supplicant.conf  # config file

	- if multiple wifi networks => add following to config. file ```priority=<number>, id_str="<string_name>"```. Example of the the network configuration:
            
            network={
		      ssid=<"network_name">
		      #psk="heslo" - delete!!!!
		      psk=f088fe5f142e02931d02c19fd14ea5e768075dbddc4b627db553b06da9aed8ea
		      }

	- to generate hexa encrypted pswd 
                  
            wpa_passphrase <"network_name">
            
      - enter password => encryption generated => copy hexa code to previous file

**Step 4:** Install git
      
      sudo apt-get install git

**Step 5:** SSH connection to the git repo
* generate ssh key
	
      ssh-keygen -t ed25519 -C "<comment>" - ssh key creator (/home/user/.ssh/id_ed25519)

* copy the key ```nano ~/.ssh/id_ed25519.pub```

* copy the ssh key by hand or cmd
* insert the key to the gitlab or github => [Full Guide](https://gitlab.fel.cvut.cz/help/ssh/README#common-steps-for-generating-an-ssh-key-pair)

**Step 6:** Install packet forwarder
* clone our repo to Raspi
	
      git clone git@gitlab.fel.cvut.cz:sudamar1/indoornavigation.git <local_git_project_directory>

* find gateway_install and execute install.sh
      - if not executable run: sudo chmod +x install.sh
      - execute ./install.sh
      - default settings are in brackets, press Enter to validate. Output example:

      Hostname [inavi-gateway]:
      Region AS1, AS2, AU, CN, EU, IN, KR, RU, US [EU]:
      Latitude in degrees [0.0]
      Longitude in degrees [0.0]:
      Altitude in meters [0]

* restart of pin 7 at our environment (path might differ)
      
      cd ../inavi-gateway/lora_gateway/ && ./restart_lgw.sh start

* make sure this is at the end of the global_conf.json

      "serv_port_down": 1700,
      "serv_port_up": 1700,
      "server_address": "localhost", <= THIS IS IMPORTANT: INSERT THE NETWORK SERVER ADDR.

* FURTHER INFO:
      - service of pktfwd saved in /lib/systemd/system/pktfwd.service
      - install.sh script removes /etc/systemd/system/multi-user.target.wants/hciuart.service

**Step 7:** [OPTIONAL STEP] By default GPS is disabled. If you want to use GPS:
* Attach the GPS antenna
      - For better GPS results the antenna must be placed outside with a clear view of the sky.
      - The antenna needs to have an open line of sight to the GPS satellites in the sky.

* modify /inavi-gateway/packet_forwarder/lora_pkt_fwd/local_conf.json and uncomment the following content:
      - Watch out for commas. The JSON file must be valid!

        "gateway_conf": {
            :
            "gps_tty_path": "/dev/ttyAMA0",
            "fake_gps": false
            :
        }

* Reboot the gateway after file local_conf.json is modified.

**Step 8:** Now packet forwarder and lora_gateway is successfully installed!

### Install Mosquitto broker (MQTT broker)
MQTT broker is required for the chirpstak network server stack and for the communication with the configuration application. The application uses MQTT over websockets, which is not default configuration of Mosquitto broker. Following steps need to be done:

**Step 1:** Download a build of libwebsockets => dependance of websocket mosquitto

      sudo apt-get install libwebsockets-dev

* FURTHER INFO:
	- had to do it on 19.2.2020 version - I dont know if always neccessary
	- check whether the libwebsock path is findible by the config.mk in mosquitto dir (e.g. [config.mk_paths.png](/docs/config.mk_paths.png))
	
            dpkg -L libwebsockets-dev  # to shows the dir where it is saved (look for liwebsockets.h !!) 
	
      - if not finable copy whole folder, where libweb.h is, to the incdir directory coded in config.mk ([previous png](/docs/config.mk_paths.png)) 
	- last installation my cmd was => sudo cp /usr/include /usr/local/include 
	- other info here: https://github.com/eclipse/mosquitto 

**Step 2:** Download a build of mosqitto sources

      git clone https://github.com/eclipse/mosquitto
      cd mosquitto

**Step 3:** In config.mk change following: 

      WITH_WEBSOCKETS:=yes
      WITH_DOCS=no # without doc

**Step 4:** Build all:

      make
	sudo make install

* exe file then in scr file!
* try to run the mosquitto exe

**Step 5:** Create Mosquitto as service 
* in mosquitto dir find the service dir

      cd service

* pick the appropriate service file (e.g. mosquitto.service.simple)
      
      mv mosquitto.service.simple mosquitto.service
      sudo cp mosquitto.service /lib/systemd/system

* copy exe to the dir which service uses
      
      sudo cp /mosquitto/src/mosquitto /usr/sbin/
      

* finnaly add a system user to operate the mosquitto (viz adding_system_user.png)
      
      sudo adduser --system mosquitto

### Install chirpstack components
Full chirpstack guide can be found [here](https://www.chirpstack.io/gateway-bridge/install/debian/)

**Step 1:**  add repo	
* run following to add the chirpstack repository
      
      sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1CE2AFD36DBCCA00
	
      sudo echo "deb https://artifacts.chirpstack.io/packages/3.x/deb stable main" | sudo tee /etc/apt/sources.list.d/chirpstack.list

	sudo apt update
	
**Step 2:** install dependancies

	sudo apt install postgresql
	sudo apt install redis-server

**Step 3:** set postgresql

	sudo -u postgres psql  # database for loraserver
	#change the dbpassword!
	create role loraserver_ns with login password 'dbpassword';  # create the chirpstack_as user
	create database loraserver_ns with owner loraserver_ns;  # create the chirpstack_as database
	\q

	psql -h localhost -U loraserver_ns -W loraserver_ns  # verification all was created
	\q

	#database for appserver
	sudo -u postgres psql
	create role chirpstack_as with login password 'dbpassword';  # change the dbpassword
	create database chirpstack_as with owner chirpstack_as;
	#enable the trigram and hstore extensions
	\c chirpstack_as
	create extension pg_trgm;
	create extension hstore;
	\q

	#verification
	psql -h localhost -U chirpstack_as -W chirpstack_as

**Step 4:** install the components
	
      sudo apt install chirpstack-gateway-bridge chirpstack-network-server
	sudo apt-get install chirpstack-application-server

**Step 5:** configuration of chirpstack components
* network server
	
      sudo nano /etc/chirpstack-network-server/chirpstack-network-server.toml

      #check the correct band name (e.g. EU868)
      #connect database e.g.:
      dsn="postgres://loraserver_ns:dbpassword @localhost/loraserver_ns?sslmode=disable"
      
* app server

      sudo nano /etc/chirpstack-application-server/chirpstack-application-server.toml

      #connect database e.g.:
      dsn="postgres://chirpstack_as:dbpassword@localhost/chirpstack_as?sslmode=disable"
      #change application_server.external_api.jwt_secret
      openssl rand -base64 32

      #create a JSON Web Token (jwt). Open a terminal and run:
      openssl rand -base64 32
      #copy and save the output. copy this jwt to the app server conf. Keep this jwt in secret! (e.g. e3+eD7zcVFJF3EFpPnM1oMj02DqUZxt5wR4IfPBpbtA=)

### Final setup

**Step 1:** Start the services ```sudo systemctl start <service_name>```

**Step 2:** Check the services ```systemctl status <service_name>```

**Step 3:** Backend connection to the APP SERVER
* go to ip:8080 (e.g. http://192.168.0.220:8080/)
* in order to use gw app server offline following has to be changed in the network-servers tab of the web ui => localhost to 127.0.0.1
* network server toml file (make sure it is not mqtt server!)=> server=http://127.0.0.1:8003
* app server toml file =>  public_host=127.0.0.1:8001
* web interface network server configuration 127.0.0.1:8000

**Step 4:** App server configuration... (WILL BE ADDED)
