#!/bin/bash
# gw start

# stop on the first sign of trouble
set -e

echo "----GATEWAY STARTUP SCRIPT----"

echo "Reseting SPI..."
cd /inavi-gateway/lora_gateway && ./reset_lgw.sh start
echo "Reset done!"

echo "Starting services"
sleep 2

# starting mosquitto
echo "starting MQTT broker"
sudo systemctl start mosquitto.service
echo "done"

# starting pkt_fwd and concentrator
echo "starting pktfwd"
sleep 2
sudo systemctl start pktfwd.service
echo "done"

#starting lora-gateway-bridge
echo "starting lora-bridge"
sudo systemctl start lora-gateway-bridge
echo "done"

#starting loraserver
echo "starting loraserver"
sudo systemctl start loraserver.service
echo "done"

#starting lora-app-server
echo "starting app-server"
sudo systemctl start lora-app-server.service
echo "done"
