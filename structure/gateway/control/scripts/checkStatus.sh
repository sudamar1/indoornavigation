#!/bin/bash
# gw status check

# stop on the first sign of trouble
set -e

echo "----SERVICE STATUS CHECK----"

# check of packet forwarder and concentrator
echo "inavi-gateway: $(systemctl is-active pktfwd.service)"

# check lora gateway bridge
echo "lora-bridge: $(systemctl is-active lora-gateway-bridge.service)"

# check  loraserver
echo "loraserver: $(systemctl is-active loraserver.service)"

# check lora app server
echo "app-server: $(systemctl is-active lora-app-server.service)"

# check mosquitto MQTT broker
echo "mqtt-broker: $(systemctl is-active mosquitto.service)"
