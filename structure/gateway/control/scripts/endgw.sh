#!/bin/bash
# gw stop
# does not work ideally loraserver service fails
# stop on the first sign of trouble
set -e

echo "----STOPING GATEWAY SERVICES----"
sleep 2

# stoping mosquitto
echo "stoping MQTT broker"
sudo systemctl stop mosquitto.service
echo "done"

# stoping pkt_fwd and concentrator
echo "stoping pktfwd"
sleep 2
sudo systemctl stop pktfwd.service
echo "done"

#stoping lora-gateway-bridge
echo "stoping lora-bridge"
sudo systemctl stop lora-gateway-bridge
echo "done"

#stoping loraserver
echo "stoping loraserver"
sudo systemctl stop loraserver.service
echo "done"

#stoping lora-app-server
echo "stoping app-server"
sudo systemctl stop lora-app-server.service
echo "done"
