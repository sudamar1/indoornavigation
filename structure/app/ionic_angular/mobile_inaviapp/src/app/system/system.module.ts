import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { SystemPageRoutingModule } from './system-routing.module';

import { SystemPage } from './system.page';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SystemPageRoutingModule
  ],
  declarations: [SystemPage]
})
export class SystemPageModule {}
