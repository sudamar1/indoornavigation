import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemPage } from './system.page';

const routes: Routes = [
  {
    path: '',
    component: SystemPage,
    children: [
      {path: 'config',
      loadChildren: () => import('./config/config.module').then( m => m.ConfigPageModule)
    },
      {path: 'settings',
      loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemPageRoutingModule {}
