import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentChangeEventDetail } from '@ionic/core';
import { PwmComponent } from './pwm/pwm.component';
import { OfdmComponent } from './ofdm/ofdm.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-modulations',
  templateUrl: './modulations.component.html',
  styleUrls: ['./modulations.component.scss'],
})
export class ModulationsComponent implements OnInit {

  @Output() public onSubmit:EventEmitter<any> = new EventEmitter<any>();
  modulationChange: Subject<void> = new Subject<void>();
  //@ViewChild(PwmComponent, {static: true}) pwmComponentRef: PwmComponent;
  //@ViewChild(OfdmComponent, {static: true}) ofdmComponentRef: OfdmComponent;

  configForm: FormGroup;
  modulationType: string = "pwm";
  freqUnits: number;
  nodesNum: number;
  variableFreq: boolean;

  constructor() {
    
  }

  ngOnInit() {
    // modulation form init
    this.configForm = new FormGroup({
        modulation: new FormControl("pwm",{
          updateOn: 'change',
          validators: [Validators.required]
        })
      });
  }

  onFormSubmit() {
    this.onSubmit.emit(this.configForm);
  }

  updateFormValuesPwm(form) {
    this.configForm.addControl('pwm', form);
    //console.log(this.configForm);
  }

  updateFormValuesOfdm(form) {
    this.configForm.addControl('ofdm', form);
    //console.log(this.configForm);
  }

  updateFormValuesOok(form) {
    this.configForm.addControl('ook', form);
    //console.log(this.configForm);
  }

  updateFormValuesSinus(form) {
    this.configForm.addControl('sin', form);
    //console.log(this.configForm);
  }

  modulationSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
    this.modulationType = (<HTMLInputElement>event.detail).value;
    this.configForm.removeControl(this.modulationType);
    //console.log("modulation change");
    //console.log(this.modulationType);
    // if (this.modulationType == 'pwm') {

    // } else if (this.modulationType == 'ofdm') {
    //   this.modulationChange.next();
    // }
    // if (this.modulationType == 'pwm'){
    //   this.configForm = this.pwmComponentRef.configFormPwm;
    //   console.log(this.configForm)
    // } else if (this.modulationType == 'ofdm') {
    //   this.configForm = this.ofdmComponentRef.configFormOfdm;
    // }
    
  }

  // clearValues(){
  
  //   //console.log(this.configForm);
  // }

  // unitSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
  //   this.freqUnits = +(<HTMLInputElement>event.detail).value;
  //   //console.log(this.freqUnits);
  //   //console.log(this.configForm);
  // }
  // toggleFreqChanged(){
  //   this.variableFreq = this.configForm.value.variableFreq;
  //   if (this.variableFreq) {
  //     this.configForm.controls['frequency'].disable();
  //     this.configForm.controls['freqUnits'].disable();
  //   } else {
  //    this.configForm.controls['frequency'].enable();
  //    this.configForm.controls['freqUnits'].enable();
  //   }
  // }

}
