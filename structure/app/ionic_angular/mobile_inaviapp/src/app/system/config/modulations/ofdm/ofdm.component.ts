import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, AfterViewInit, AfterContentChecked } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentChangeEventDetail } from '@ionic/core';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-ofdm',
  templateUrl: './ofdm.component.html',
  styleUrls: ['./ofdm.component.scss'],
})
export class OfdmComponent implements OnInit, OnDestroy {
  
  @Input() modForm: FormGroup;
  //@Input() modulationEvent: Observable<void>;
  @Output() onOfdmFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  private modulationChangeSubscription: Subscription;
  configFormOfdm: FormGroup;
  freqUnits: number;
  nodesNum: number;
  variableFreq: boolean;

  constructor() { }

  ngOnInit() {
    this.configFormOfdm = new FormGroup({
      frequency: new FormControl(100, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      freqUnits: new FormControl("0",{
        updateOn: 'change',
        validators: [Validators.required]
      }),
      nodesNum: new FormControl(1, {
        updateOn: 'change',
        validators: [
          Validators.required,
          Validators.min(1)]
      }),
      variableFreq: new FormControl(false, {
        updateOn: 'change',
        validators: []
      }),
      ofdmGuardInterval: new FormControl(5, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
    });
    //console.log("onit")
    this.addVariablesToModulationForm();
    this.onValueChange();
    //console.log(this.configFormOfdm);
    // this.modulationChangeSubscription = this.modulationEvent.subscribe(() => {
    //   this.addVariablesToModulationForm()
    //   console.log('ofdm sub')
    //   });
    //this.addVariablesToModulationForm();
    //console.log(this.modulationForm);
    //console.log('onOnit Ofdm');
  }

  // ngAfterContentChecked() {
  //   this.modulationChangeSubscription = this.modulationEvent.subscribe(() => {
  //     this.addVariablesToModulationForm()
  //     console.log('ofdm sub')
  //     });
  // }

  ngOnDestroy() {
    //this.modulationChangeSubscription.unsubscribe();
    //console.log("destroj")
  }
  
  onValueChange(): void {
    this.configFormOfdm.valueChanges.subscribe( () =>{
      //console.log(changedValue);
      this.addVariablesToModulationForm()
      //console.log(this.modulationForm);
    })
  }

  addVariablesToModulationForm() {
    //console.log("variables")
    //this.modulationForm.addControl('child', this.configFormOfdm);
    this.onOfdmFormChange.emit(this.configFormOfdm);
  }
  
  unitSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
    this.freqUnits = +(<HTMLInputElement>event.detail).value;
    //console.log(this.freqUnits);
    //console.log(this.configForm);
  }

  toggleFreqChanged(){
    this.variableFreq = this.configFormOfdm.value.variableFreq;
    if (this.variableFreq) {
      this.configFormOfdm.controls['frequency'].disable();
      this.configFormOfdm.controls['freqUnits'].disable();
    } else {
     this.configFormOfdm.controls['frequency'].enable();
     this.configFormOfdm.controls['freqUnits'].enable();
    }
  }

}
