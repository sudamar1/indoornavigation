import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentChangeEventDetail } from '@ionic/core';

@Component({
  selector: 'app-sinus',
  templateUrl: './sinus.component.html',
  styleUrls: ['./sinus.component.scss'],
})
export class SinusComponent implements OnInit {

  //@Input() modulationForm: FormGroup;
  @Output() onSinusFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  
  configFormSinus: FormGroup;
  freqUnits: number;
  nodesNum: number;
  variableFreq: boolean;

  constructor() { }

  ngOnInit() {
    this.configFormSinus = new FormGroup({
      frequency: new FormControl(100, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      freqUnits: new FormControl("0",{
        updateOn: 'change',
        validators: [Validators.required]
      }),
      amp: new FormControl(2, {
        updateOn: 'blur',
        validators: [
          Validators.required,
          Validators.min(0.1),
          Validators.max(3)]
      }),
      phase: new FormControl(0, {
        updateOn: 'blur',
        validators: [
          Validators.required,
          Validators.min(0),
          Validators.max(359.9)]
      }),
      nodesNum: new FormControl(1, {
        updateOn: 'change',
        validators: [
          Validators.required,
          Validators.min(1)]
      }),
      variableFreq: new FormControl(false, {
        updateOn: 'change',
        validators: []
      }),
    });

    this.addVariablesToModulationForm();
    //console.log(this.modulationForm);
    this.onValueChange();
  }
  
  onValueChange(): void {
    this.configFormSinus.valueChanges.subscribe( () =>{
      //console.log(changedValue);
      this.addVariablesToModulationForm()
      //console.log(this.modulationForm);
    })
  }

  addVariablesToModulationForm() {
    //console.log("variables")
    //this.modulationForm.addControl('child', this.configFormSinus);
    this.onSinusFormChange.emit(this.configFormSinus);
  }
  
  unitSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
    this.freqUnits = +(<HTMLInputElement>event.detail).value;
    //console.log(this.freqUnits);
    //console.log(this.configForm);
  }
  toggleFreqChanged(){
    this.variableFreq = this.configFormSinus.value.variableFreq;
    if (this.variableFreq) {
      this.configFormSinus.controls['frequency'].disable();
      this.configFormSinus.controls['freqUnits'].disable();
    } else {
     this.configFormSinus.controls['frequency'].enable();
     this.configFormSinus.controls['freqUnits'].enable();
    }
  }

}
