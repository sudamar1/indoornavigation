import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ConfigPageRoutingModule } from './config-routing.module';
import { ConfigPage } from './config.page';
import { ModulationsComponent } from './modulations/modulations.component';
import { PwmComponent } from './modulations/pwm/pwm.component';
import { OfdmComponent } from './modulations/ofdm/ofdm.component';
import { OokComponent } from './modulations/ook/ook.component';
import { SinusComponent } from './modulations/sinus/sinus.component';

// MQTT
import { MqttModule, IMqttServiceOptions } from 'ngx-mqtt';



export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  connectOnCreate: false,
  hostname: '192.168.0.220',
  port: 9001,
  path: '/mqtt',
  protocol: "ws"
}

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ConfigPageRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS),
  ],
  declarations: [
    ConfigPage,
    ModulationsComponent,
    PwmComponent,
    OfdmComponent,
    OokComponent,
    SinusComponent,
  ]
})
export class ConfigPageModule {}
