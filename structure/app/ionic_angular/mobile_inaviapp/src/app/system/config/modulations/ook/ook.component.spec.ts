import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OokComponent } from './ook.component';

describe('OokComponent', () => {
  let component: OokComponent;
  let fixture: ComponentFixture<OokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OokComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
