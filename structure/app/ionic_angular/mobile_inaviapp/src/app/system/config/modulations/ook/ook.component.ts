import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentChangeEventDetail } from '@ionic/core';

@Component({
  selector: 'app-ook',
  templateUrl: './ook.component.html',
  styleUrls: ['./ook.component.scss'],
})
export class OokComponent implements OnInit {

  @Input() modulationForm: FormGroup;
  @Output() onOokFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  
  configFormOok: FormGroup;
  freqUnits: number;
  nodesNum: number;
  variableFreq: boolean;

  constructor() { }

  ngOnInit() {
    this.configFormOok = new FormGroup({
      frequency: new FormControl(100, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      freqUnits: new FormControl("0",{
        updateOn: 'change',
        validators: [Validators.required]
      }),
      logOneAmp: new FormControl(3, {
        updateOn: 'blur',
        validators: [
          Validators.required,
          Validators.max(3)]
      }),
      logZeroAmp: new FormControl(0, {
        updateOn: 'blur',
        validators: [
          Validators.required,
          Validators.min(-3)]
      }),
      nodesNum: new FormControl(1, {
        updateOn: 'change',
        validators: [
          Validators.required,
          Validators.min(1)]
      }),
      variableFreq: new FormControl(false, {
        updateOn: 'change',
        validators: []
      }),
    });

    this.addVariablesToModulationForm();
    //console.log(this.modulationForm);
    this.onValueChange();
  }
  
  onValueChange(): void {
    this.configFormOok.valueChanges.subscribe( () => {
      //console.log(changedValue);
      this.addVariablesToModulationForm()
      //console.log(this.modulationForm);
    })
  }

  addVariablesToModulationForm() {
    //console.log("variables")
    //this.modulationForm.addControl('child', this.configFormOok);
    this.onOokFormChange.emit(this.configFormOok);
  }
  
  unitSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
    this.freqUnits = +(<HTMLInputElement>event.detail).value;
    //console.log(this.freqUnits);
    //console.log(this.configForm);
  }
  toggleFreqChanged(){
    this.variableFreq = this.configFormOok.value.variableFreq;
    if (this.variableFreq) {
      this.configFormOok.controls['frequency'].disable();
      this.configFormOok.controls['freqUnits'].disable();
    } else {
     this.configFormOok.controls['frequency'].enable();
     this.configFormOok.controls['freqUnits'].enable();
    }
  }

}
