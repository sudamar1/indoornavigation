import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SegmentChangeEventDetail } from '@ionic/core';


@Component({
  selector: 'app-pwm',
  templateUrl: './pwm.component.html',
  styleUrls: ['./pwm.component.scss'],
})
export class PwmComponent implements OnInit {
  
  @Input() modulationForm: FormGroup;
  @Output() onPwmFormChange: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  
  configFormPwm: FormGroup;
  freqUnits: number;
  nodesNum: number;
  variableFreq: boolean;

  constructor() { }

  ngOnInit() {
    this.configFormPwm = new FormGroup({
      frequency: new FormControl(100, {
        updateOn: 'blur',
        validators: [Validators.required]
      }),
      freqUnits: new FormControl("0",{
        updateOn: 'change',
        validators: [Validators.required]
      }),
      dutyCycle: new FormControl(0.5, {
        updateOn: 'blur',
        validators: [
          Validators.required,
          Validators.min(0.01),
          Validators.max(0.99)]
      }),
      nodesNum: new FormControl(1, {
        updateOn: 'change',
        validators: [
          Validators.required,
          Validators.min(1)]
      }),
      variableFreq: new FormControl(false, {
        updateOn: 'change',
        validators: []
      }),
    });

    this.addVariablesToModulationForm();
    //console.log(this.modulationForm);
    this.onValueChange();
  }
  
  onValueChange(): void {
    this.configFormPwm.valueChanges.subscribe( () =>{
      //console.log(changedValue);
      this.addVariablesToModulationForm()
      //console.log(this.modulationForm);
    })
  }

  addVariablesToModulationForm() {
    //console.log("variables")
    //this.modulationForm.addControl('child', this.configFormPwm);
    this.onPwmFormChange.emit(this.configFormPwm);
  }
  
  unitSegChanged(event: CustomEvent<SegmentChangeEventDetail>) {
    this.freqUnits = +(<HTMLInputElement>event.detail).value;
    //console.log(this.freqUnits);
    //console.log(this.configForm);
  }
  toggleFreqChanged(){
    this.variableFreq = this.configFormPwm.value.variableFreq;
    if (this.variableFreq) {
      this.configFormPwm.controls['frequency'].disable();
      this.configFormPwm.controls['freqUnits'].disable();
    } else {
     this.configFormPwm.controls['frequency'].enable();
     this.configFormPwm.controls['freqUnits'].enable();
    }
  }

}
