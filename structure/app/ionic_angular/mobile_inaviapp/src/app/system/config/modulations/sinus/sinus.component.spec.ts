import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SinusComponent } from './sinus.component';

describe('SinusComponent', () => {
  let component: SinusComponent;
  let fixture: ComponentFixture<SinusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinusComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SinusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
