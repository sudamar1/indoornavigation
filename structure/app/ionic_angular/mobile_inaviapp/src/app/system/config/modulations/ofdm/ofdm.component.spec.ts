import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OfdmComponent } from './ofdm.component';

describe('OfdmComponent', () => {
  let component: OfdmComponent;
  let fixture: ComponentFixture<OfdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfdmComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OfdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
