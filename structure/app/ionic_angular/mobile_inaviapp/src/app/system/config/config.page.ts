import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs'; // observibles
import { IMqttMessage, MqttService, MQTT_SERVICE_OPTIONS, IMqttServiceOptions } from 'ngx-mqtt';
import { ConfigMessageService } from './config-message.service';
import { ConfigMessage } from './config-message.model';
import { FormGroup } from '@angular/forms';
import { Buffer } from 'buffer';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
  providers: [MqttService]
})
export class ConfigPage implements OnInit, OnDestroy {
  // MQTT variables
  private subscription: Subscription;
  topicName: string;
  stringMsg: string;
  mqttMessage: Object;
  mqttState;
  isConnected: boolean = false;
  @ViewChild('msglog', { static: true }) msglog: ElementRef;
  configForm: FormGroup;
  MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
    connectOnCreate: false,
    hostname: '192.168.0.220',
    port: 9001,
    path: '/mqtt',
    protocol: "ws"
  };
  dummyMsg: Object = {
    frequency: 1000,
    dutyCycle: 0.5,
    modulationType: "pwm"
  };


  constructor(private mqttService: MqttService,
    private configMessageService: ConfigMessageService) {

    this.mqttService.state.subscribe(() => {
      if (this.mqttService.state.value == 0) {
        this.mqttState = "Disconnected";
      } else if (this.mqttService.state.value == 1){
        this.mqttState = "Connecting";
      } else {
        this.mqttState = "Connected"
      }

    });
  }

  ngOnInit() {
    console.log(this.mqttService.state.value == 0 ? "disconnected" : "other");
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  configureNaviSystem(form: FormGroup) {
    this.configForm = form;
    this.mqttMessageCreator();
    console.log(this.mqttMessage);
    let mqttBufferMessage = this.jsonToBuffer(this.mqttMessage);
    this.topicName = "application/1/device/47db558000360024/tx";
    this.mqttService.unsafePublish(this.topicName, mqttBufferMessage,
    { qos: 1, retain: true });
    console.log("Message sent!");
  }

  mqttMessageCreator() {
    const payload = this.configMessageService.getMessageInit(this.configForm);
    this.mqttMessage = {
      confirmed: true,
      fPort: 8,
      object: payload
    }
    //console.log(this.mqttMessage);
  }

  //MQTT
  mqttConnect() {
    console.log("Connecting to the broker " + this.MQTT_SERVICE_OPTIONS.hostname + ":" + this.MQTT_SERVICE_OPTIONS.port);
    console.log(this.MQTT_SERVICE_OPTIONS);
    this.mqttService.connect(this.MQTT_SERVICE_OPTIONS);
    console.log("Status: " + this.mqttStatusCheck());
    this.mqttState;
  }

  mqttDisconnect() {
    console.log("Disconnecting from the broker " + this.MQTT_SERVICE_OPTIONS.hostname + ":" + this.MQTT_SERVICE_OPTIONS.port);
    this.mqttService.disconnect(true);
    console.log("Status: " + this.mqttStatusCheck());
  }

  mqttStatusCheck() {
    const status = this.mqttService.state.value;
    const broker = MQTT_SERVICE_OPTIONS.hostname;
    const port = MQTT_SERVICE_OPTIONS.port;
    let strStatus;
    if (status == 0){
      strStatus = "Disconnected";
    } else if (status == 2) {
      strStatus = "Connected to " + broker + ":" + port;
    } else {
      strStatus = "Connecting to " + broker + ":" + port;
    }
    return strStatus;
  }

  // json to buffUnit8Array convertion
  jsonToBufferArray(message: object) {
    var tempStr = JSON.stringify(message, null, 0);
    var bufferArray = new Uint8Array(tempStr.length);
    for (var i = 0; i < tempStr.length; i++) {
      bufferArray[i] = tempStr.charCodeAt(i);
    }
    return bufferArray;
  }

// buffUnit8Array to json convertion
  bufferArrayToJson(bufferArray: any) {
    var tempStr = "";
    for (var i = 0; i < bufferArray.length; i++) {
      tempStr += String.fromCharCode(parseInt(bufferArray[i]));
    }
    return JSON.parse(tempStr);
  }

  jsonToBuffer(message: Object) {
    let buffer = Buffer.from(JSON.stringify(message));
    //console.log(buffer);
    let temp = JSON.parse(buffer.toString());
    //console.log(temp);
    return buffer;
  }

  // subscribeNewTopic(): void {
  //   console.log("A new topic " + "'"+this.topicName+"'" + " is being subscribed!");
  //   this.subscription = this.mqttService.observe(this.topicName).subscribe(
  //     (message: IMqttMessage) => {
  //       var msg = this.jsonToBufferArray(this.mqttMessage);
  //       msg = message.payload;
  //       console.log('msg: ', message);
  //       //this.logMsg('Message: ' + message.payload.toString() + '<br> for topic: '
  //       //  + message.topic);
  //     }
  //   );
  //   //this.logMsg('subscribed to topic: ' + this.topicName);
  // }

  // sendMsg(): void {
  //   this.mqttService.unsafePublish(this.topicName, this.jsonToBuffer(this.dummyMsg),
  //   { qos: 1, retain: true });
  //   console.log("Message sent!");
  // }
  
  logMsg(message): void {
    this.msglog.nativeElement.innerHTML += '<br><hr>' + message;
  }

  clear(): void {
    this.msglog.nativeElement.innerHTML = '';
  }

}
