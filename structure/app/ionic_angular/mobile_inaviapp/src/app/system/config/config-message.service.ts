import { Injectable } from '@angular/core';
import { ConfigMessage } from './config-message.model';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ConfigMessageService {
  // Dummy data
  // private confMessage: ConfigMessage = {
  //   frequency: 50,
  //   dutyCycle: 0.5,
  //   modulationType: "PWM"
  // }
  
  // get getConfigMsg() {
  //   return this.confMessage;
  // }

  // getMessageInit(form: FormGroup) {
  //     this.confMessage = new ConfigMessage(
  //     (form.value.child.frequency)*10**(+form.value.child.freqUnits),
  //     form.value.child.dutyCycle,
  //     form.value.modulation);
  //     return this.confMessage;
  // }

  constructor() { }

  getMessageInit(form: FormGroup) {
    let modulationType = form.value.modulation;
    let confMessage;
    //console.log("form")
    //console.log(form);
    if (modulationType == 'pwm') {
      confMessage = {
        modulationType: modulationType,
        frequency: form.value.pwm.variableFreq ? true : (form.value.pwm.frequency)*10**(+form.value.pwm.freqUnits),
        dutyCycle: form.value.pwm.dutyCycle,
        }
    } else if (modulationType == 'ofdm'){
      confMessage = {
        modulationType: modulationType,
        frequency: form.value.ofdm.variableFreq ? true : (form.value.ofdm.frequency)*10**(+form.value.ofdm.freqUnits),
        guardInterval: form.value.ofdm.ofdmGuardInterval,
      }
    } else if (modulationType == 'sin'){
      confMessage = {
        modulationType: modulationType,
        frequency: form.value.sin.variableFreq ? true : (form.value.sin.frequency)*10**(+form.value.sin.freqUnits),
        amp: form.value.sin.amp,
        phase: form.value.sin.phase,
      }
    }else if (modulationType == 'ook'){
      confMessage = {
        modulationType: modulationType,
        frequency: form.value.ook.variableFreq ? true : (form.value.ook.frequency)*10**(+form.value.ook.freqUnits),
        logOne: form.value.ook.logOneAmp,
        logZero: form.value.ook.logZeroAmp,
      }
    }
    return confMessage;
  }
    
}
