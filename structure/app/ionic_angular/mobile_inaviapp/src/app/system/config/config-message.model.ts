export class ConfigMessage {
    
    // constructor(
    //     public frequency: number,
    //     public dutyCycle: number,
    //     public modulationType: string,
    // ) {}

    constructor(
        public frequency: number,
        public dutyCycle: number,
        public modulationType: string,
        public guardInterval: number,
        public logOne: number,
        public logZero: number
    ) {}
}