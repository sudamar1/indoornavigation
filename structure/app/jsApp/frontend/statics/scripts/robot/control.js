/** Summary.
 *
 * Implements frontend for movement, LED and camera control.
 *
 * @link   https://dronebotworkshop.com/esp32cam-robot-car/#esp32cam-robotino
 * @file   This files does not define class.
 * @author Stepan Bosak.
 * @since  2022.04.25
 */
/ jshint {inline configuration here} */;
import { sendPostRequest } from "../utils/requests.js";
import { writeLog } from "../utils/render.js";
import { ipFormatValidator, connectionCheck } from "./api.js";

let flashOn = 0; //< variable for turning led on and off

/**
 * Function for switching the flashligh on and off through HTTP.
 * @returns void
 */
// led flash controller
export async function flashLight() {
  const ip = document.getElementById("rob_ip").value;
  // check for connection first
  if (!connectionCheck() || !ipFormatValidator(ip)) return;

  // get ID intensity
  const intensity = document.getElementById("flash-intensity").value;
  // change flash state
  flashOn = +!flashOn;
  const data = [flashOn, intensity];
  //send request
  const resp = await sendPostRequest(`http://${ip}/led-on-off`, data, {
    "Content-Type": "text/plain",
  });
  console.log("Response text: " + resp.responseText);
  if (resp.responseText == String(flashOn)) {
    // get button
    const flashButton = document.getElementById("flashButton");
    if (flashOn == 0) {
      //change the button background
      flashButton.classList.remove("on");
    } else {
      flashButton.classList.add("on");
    }
  } else {
    writeLog("LED Request Failed");
  }
}

/**
 * Sends motor movement request to the robot.
 * @param {*} Number Number from 1 - 4 defines the movement function.
 * @returns void
 */
export async function sendMotorData(buttonID) {
  const ip = document.getElementById("rob_ip").value;
  // check for connection first
  if (!connectionCheck() || !ipFormatValidator(ip)) return;

  // get ID speed
  const speed = Number(document.getElementById("motor-speed").value);
  // data array for post request
  const data = [buttonID, speed];

  const resp = await sendPostRequest(`http://${ip}/motors`, data, {
    "Content-Type": "text/plain",
  });

  if (resp.responseText == "OK") writeLog("Robot moved!");
  else writeLog("Request Failed");
}

/**
 * Function that sends JSON with camera configuration parameters.
 * @returns void
 */
export async function setCamera() {
  // create post request
  const ip = document.getElementById("rob_ip").value;

  if (!ipFormatValidator(ip) || !connectionCheck()) return;

  // retrieve value of all settings elements
  const form = document.getElementById("camera_settings_form");
  let cameraArray = {};

  //go through form and get all elements
  for (let i = 0; i < form.elements.length; i++) {
    let e = form.elements[i];
    cameraArray[String(e.id)] = encodeURIComponent(
      e.getAttribute("type") == "checkbox" ? +e.checked : e.value
    );
  }

  //create json string
  let cameraJson = JSON.stringify(cameraArray);

  const resp = await sendPostRequest(
    `http://${ip}/camera-settings`,
    cameraJson,
    { "Content-Type": "application/json" }
  );

  if (resp.responseText == "OK") writeLog(cameraJson);
  else writeLog("Set camera request failed!");
}
