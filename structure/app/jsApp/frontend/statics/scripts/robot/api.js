/** Summary.
 *
 * Implements robot conectivity and image fetching.
 *
 * @link   https://dronebotworkshop.com/esp32cam-robot-car/#esp32cam-robotino
 * @file   This files does not define class.
 * @author Stepan Bosak.
 * @since  2022.04.25
 */
import { sendPostRequest, sendGetRequest } from "../utils/requests.js";
import { removeSpinLoader, writeLog } from "../utils/render.js";

let timerID; //< checks connection of the robot periodically
let fetchTimerID; //< time for fetching images periodically
const connectionCheckPeriod = 5000; //< period of connection request
let connected = false; //< connected for button and led

/**
 * Checks if the app is connected or not. Calls appropriate function.
 */
export async function connect() {
  if (!connected) connectToRobot();
  else disconnectFromRobot();
}

/**
 * Function, which sends get request to backend. To retrieve an image.
 * @returns void
 */
export async function fetchImage() {
  const ip = document.getElementById("rob_ip").value;
  if (!connectionCheck() || !ipFormatValidator(ip)) return;

  const resp = await sendGetRequest("/robot/fetch", {
    "Content-type": "image/jpeg",
  });
  if (resp.status == 200) {
    displayImage(resp.responseText);
    resetConnectionTmr();
  } else {
    writeLog(resp.responseText);
    writeLog(resp.status);
    disconnectFromRobot();
  }
}

/**
 * Function for periodic request on the image.
 * @returns void
 */
export function setPeriod() {
  const ip = document.getElementById("rob_ip").value;
  if (!connectionCheck() || !ipFormatValidator(ip)) {
    return;
  }
  const period = document.getElementById("img_t").value;
  if (!isNaN(period) && period > 0.01 && period != "") {
    fetchTimerID = setInterval(fetchImage, period * 1000);
    const p = document.getElementsByClassName("img_ctrl")[0].querySelector("p");
    p.innerHTML = `${period} s`;
    writeLog(`Image fetch period set: ${period} s`);
    document.getElementById("stop_img").addEventListener("click", () => {
      clearInterval(fetchTimerID);
      resetConnectionTmr();
      p.innerHTML = "";
      writeLog(`Image fetch period cleared!`);
    });
  } else alert("Wrong period! Period must be a number > 0.01s!");
}

function displayImage(img) {
  const img_div = document.getElementById("node_detect_img");
  img_div.innerHTML = `<img src="data:image/jpeg;base64,${img}"/>`;
}

/**
 *
 * @param {*} IpAddress IP address to validate.
 * @returns Bool Is the IP adress valid (True) or not (False)?
 */
export function ipFormatValidator(ipAdd) {
  if (
    /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
      ipAdd
    )
  ) {
    return true;
  }
  alert("You have entered an invalid IP address!");
  disconnectFromRobot();
  return false;
}

/**
 * Asks the status of the robot on requested IP address. Sends post request to backend.
 * @returns void
 */
export async function connectToRobot() {
  // check if window was not closed
  if (document.getElementById("rob_ip") == null) {
    connected = false;
    clearConnectionTmr();
    clearInterval(fetchTimerID);
    return;
  }
  // fetch the IP
  const ip = document.getElementById("rob_ip").value;
  if (ipFormatValidator(ip)) {
    // send post request to backend
    const resp = await sendPostRequest("/robot/connection", ip, {
      "Content-type": "text/html",
    });
    removeSpinLoader();

    if (resp.responseText == "Robot alive") {
      //if the robot exists within the network
      document.getElementById("rob_led").className = "green_led";
      document.getElementById("rob_conn").textContent = "DISCONNECT";
      connected = true;
      // create connection checker every 5s
      startConnectionTmr();
      clearInterval(fetchTimerID);
    } else {
      disconnectFromRobot();
    }
    return;
  }
}

/**
 * Function which clears periodic timers and resets the connetion button with signal led.
 */
function disconnectFromRobot() {
  document.getElementById("rob_led").className = "red_led";
  document.getElementById("rob_conn").textContent = "CONNECT";
  connected = false;
  writeLog(`Robot disconnected!`);
  // stop checking
  clearConnectionTmr();
  clearInterval(fetchTimerID);
  removeSpinLoader();
}

/**
 * Sets timeout and function for the connection check.
 */
function startConnectionTmr() {
  timerID = setTimeout(connectToRobot, connectionCheckPeriod);
}

/**
 * Stops and clears the connection timer.
 */
export function clearConnectionTmr() {
  clearTimeout(timerID);
}

/**
 * Resets the connection timer.
 */
export function resetConnectionTmr() {
  clearTimeout(timerID);
  timerID = setTimeout(connectToRobot, connectionCheckPeriod);
}

/**
 * Checks if the user connected the robot to the application.
 */
export function connectionCheck() {
  // check if window was not closed
  if (document.getElementById("rob_ip") == null) {
    clearConnectionTmr();
    clearInterval(fetchTimerID);
    return false;
  }

  // check for connection first
  if (document.getElementById("rob_led").className != "green_led") {
    alert("Check robot connection first!");
    clearInterval(fetchTimerID);
    clearConnectionTmr();
    return false;
  } else {
    resetConnectionTmr();
    return true;
  }
}
