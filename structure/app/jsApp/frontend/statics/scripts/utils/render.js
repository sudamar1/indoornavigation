/**
 * This file implements Single-Page Application rendering.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


import * as robControl from '../robot/control.js'


/* nav buttons HTML elements */ 
export const nav_config = document.getElementById("configuration");
export const nav_mqtt = document.getElementById("mqtt_test");
export const nav_add_nodes = document.getElementById("add_node");
export const nav_del_nodes = document.getElementById("del_node");
export const nav_node_detect = document.getElementById("node_detection");
export const nav_docs = document.getElementById("docs");
const renderHook = document.getElementById("config");
let draggable_node = null;


/* CONFIGURATION PAGE */

/**
 * Renders the configuration page with default settings.
 * @returns {HTMLElement}       Element containing the configuration page
 */
export function renderConfigurationPage() {
    renderHook.innerHTML = '';
    activatePageElement(nav_config);
    changeConfigLayout(renderHook, "grid-item-config-configuration");
    const div = document.createElement('div');
    const title = document.createElement('div')
    
    div.className = 'conf_options';
    div.id = 'mod_options'
    title.className= 'conf_title';

    title.innerHTML = '<h3>System Configuration</h3>'
    div.innerHTML = `
            <button id="pwm_op" class="btn_option">PWM</button>
            <button id="sin_op" class="btn_option">SIN</button>
            `;
            // <button id="custom_op" class="btn_option">CUSTOM</button>
        

    renderHook.append(title);
    renderHook.append(div);
    // const sin = sinConfiguration();
    const defaultConf = pwmConfiguration();
    // addModulationEventListeners();
    updateDragListeners();
    // return sin
    return defaultConf
}

/**
 * Creates SIN configuration element.
 * @returns {HTMLElement}       SIN configuration HTML element
 */
export function sinConfiguration(){
    const sin = document.createElement("div");
    sin.className = 'sin';
    sin.id = 'sin';
    sin.innerHTML = `
        <form id="form_configuration_param" class="configuration_param">
            <div class="conf_param">
                <div class=form__group>
                    <input id="sin_f" class="form__input" type="number" placeholder="Freq" step="1" min="0">
                    <label for="sin_f" class="form__label">Frequency [Hz]</label>
                </div>
                <input id="sin_f_fix" type="checkbox" value="true" checked>
                <label for="sin_f_fix">Fixed</label>
                <div class=form__group>
                    <input id="sin_f_step" class="form__input" type="text" placeholder="Fstep">
                    <label for="sin_f_step" class="form__label">Step</label>
                </div>
            </div>
            <div class="conf_param">
                <div class=form__group>
                    <input id="sin_a" class="form__input" type="number" placeholder="Amp" min="1">
                    <label for="sin_a" class="form__label">Amplitude [%]</label>
                </div>
                <input id="sin_a_fix" type="checkbox" value="true" checked>
                <label for="sin_a_fix">Fixed</label>
                <div class=form__group>
                    <input id="sin_a_step" class="form__input" type="text" placeholder="Fstep">
                    <label for="sin_a_step" class="form__label">Step</label>
                </div>
            </div>
            <div class="conf_param">
                <div class=form__group>
                    <input id="sin_p" class="form__input" type="number" placeholder="Phase" min="0">
                    <label for="sin_p" class="form__label">Phase [deg]</label>
                </div>
                <input id="sin_p_fix" type="checkbox" value="true" checked>
                <label for="sin_p_fix">Fixed</label>
                <div class=form__group>
                    <input id="sin_p_step" class="form__input" type="text" placeholder="Fstep">
                    <label for="sin_p_step" class="form__label">Step</label>
                </div>
            </div>
            <div class="conf_param">
                <div class=form__group>
                    <input id="sin_qos" class="form__input" type="number" value="0" placeholder="QoS" min="0">
                    <label for="sin_qos" class="form__label">QoS</label>
                </div>
                <button class="btn" type="submit" value="Submit">Configure</button>
            </div>
        </form>
        `;
    activateModulationOption('sin');
    renderHook.append(sin);

    return sin
}

/**
 * Creates PWM configuration element.
 * @returns {HTMLElement}       PWM configuration HTML element
 */
export function pwmConfiguration(){
    const pwm = document.createElement("div");
    pwm.className = 'pwm';
    pwm.id = 'pwm';
    pwm.innerHTML = `
        <form id="form_configuration_param" class="configuration_param">
            <div class="conf_param">
                <div class=form__group>
                    <input id="pwm_f" class="form__input" type="number" placeholder="Freq" step="1" min="0">
                    <label for="pwm_f" class="form__label">Frequency [Hz]</label>
                </div>
                <input id="pwm_f_fix" type="checkbox" value="true" checked>
                <label for="pwm_f_fix">Fixed</label>
                <div class=form__group>
                    <input id="pwm_f_step" class="form__input" type="text" placeholder="Fstep">
                    <label for="pwm_f_step" class="form__label">Step</label>
                </div>
            </div>
            <div class="conf_param">
                <div class=form__group>
                    <input id="pwm_d" class="form__input" type="number" placeholder="Duty" min="0">
                    <label for="pwm_d" class="form__label">Duty Cycle [%]</label>
                </div>
                <input id="pwm_d_fix" type="checkbox" value="true" checked>
                <label for="sin_d_fix">Fixed</label>
                <div class=form__group>
                    <input id="pwm_d_step" class="form__input" type="text" placeholder="Fstep">
                    <label for="pwm_d_step" class="form__label">Step</label>
                </div>
            </div>
            <div class="conf_param">
                <div class=form__group>
                    <input id="pwm_qos" class="form__input" type="number" value="0" placeholder="QoS" min="0">
                    <label for="pwm_qos" class="form__label">QoS</label>
                </div>
                <button class="btn" type="submit" value="Submit">Configure</button>
            </div>       
        </form>
        `;
    activateModulationOption('pwm');
    renderHook.append(pwm);

    return pwm
}

/**
 * Creates Custom configuration element.
 * @returns 
 */
export function customConfiguration(){
    const custom = document.createElement("div");
    custom.className = 'custom';
    custom.id = 'custom';
    custom.innerHTML = `
    <form id='custom' class='custom'>
        <div class="conf_param">
            <div class=form__group>
                <textarea id='custom_code' class='form__input' rows="9" cols="100"></textarea>
            </div>
        </div>
        <button class="btn" type="submit" value="Submit">Configure</button>
    </form>
    `;
    activateModulationOption('custom');
    renderHook.append(custom);
    return custom
}

/**
 * Activates selected modulation option.
 * @param {String} activeOp     Option to activate
 */
function activateModulationOption(activeOp){
    const ops = document.getElementById('mod_options').querySelectorAll('button');
    ops.forEach(btn => {
        btn.classList.remove('active_op'); 
    });
    document.getElementById(`${activeOp}_op`).classList.add('active_op');
}

/**
 * Remove previous modulation configuration.
 */
export function removePreviusModOption() {
    renderHook.lastChild.remove();
}


/* MQTT TEST PAGE */

/**
 * Renders MQTT TEST page.
 */
export function renderMqttTestPage() {
    renderHook.innerHTML = '';
    activatePageElement(nav_mqtt);
    changeConfigLayout(renderHook, "grid-item-config-mqtt_test");
    const title = document.createElement('div')
    title.className = 'conf_title'
    const subscribe = document.createElement('div');
    const publish = document.createElement('div')
    subscribe.className = 'mqtt_subscribe';
    publish.className = 'mqtt_publish'
    title.innerHTML = '<h3>MQTT TEST</h3>'

    subscribe.innerHTML = `
        <form id="form_test_subscribe">
            <div class="form__group">
                <input id="sub_topic" class="form__input" type="text" id="sub-topic" placeholder="Topic">
                <label for="sub_topic" class="form__label">Subscribe Topic</label>
            </div>
            <div class="form__group">
                <input id="sub_qos" class="form__input" type="text" name="sub-Qos" value="0" placeholder="QoS">
                <label for="sub_qos" class="form__label">QoS</label>
            </div>
            <button id="sub" class="btn" type="submit" value="Subscribe">Subscribe</button>
        </form>
        `;
    publish.innerHTML= `
        <form id="form_test_publish">
            <div class="form__group">
                <input id="pub_msg" class="form__input" type="text" name="message" placeholder="Payload">
                <label for="pub_msg" class="form__label">Message content</label>
            </div>
            <div class="form__group">
                <input id="pub_topic" class="form__input" type="text" name="publishTopic" placeholder="Topic">
                <label for="pub_topic" class="form__label">Publish Topic</label>
            </div>
            <div class="form__group">
                <input id="pub_qos" class="form__input" type="text" name="publishQos" value="0" placeholder="QoS">
                <label for="pub_qos" class="form__label">QoS</label>
            </div>
            <div class="conf_param">
                <input id="pub_retain" class="check__input" type="checkbox" value="true">
                <label for="pub_retain" class="check__label">Retain Message</label>
            </div>
            <button id="pub" class="btn" type="submit" value="Submit">Publish</button>
        </form>
        `;
    renderHook.append(title);
    renderHook.append(subscribe);
    renderHook.append(publish);
}

/**
 * Renders available nodes in the system.
 * @param {HTMLElement} el     Element to render the nodes 
 * @param {JSON}       nodes   Dictionary of available nodes objects
 * @returns {JSON}             List of available nodes
 */
export function renderAvailableNodes(el, nodes){
    el.innerHTML = ''
    Object.entries(nodes).forEach(([key, _]) => {
        let node = document.createElement('button')
        node.className = "node";
        node.draggable = "true";
        node.id = key;
        node.innerHTML = key;
        el.append(node);    
    });
    updateDragListeners();
    
    return nodes;
}

/**
 * Render detect node popup window.
 * @returns {Array}     [popupWin, content, body] [Popup window MTML element, popup window content, application body]
 */
export function renderDetectNodePage() {
    const body = document.querySelector('body');
    const renderHook = body.getElementsByClassName('grid-container')[0];
    const popupWin = document.createElement('div');
    const overlay = document.createElement('div');
    const content = document.createElement('div');
    popupWin.className = "detect_win";
    popupWin.id = 'popup_win';
    overlay.className = "node_overlay"
    content.className = "detect_win_content";
    content.innerHTML = `
        <h3>Detect Nodes</h3>
        <div class="detect_node_content">
            <div id="node_detect_img" class="img"><p>Check the Robot connection.\nStart fetching images</p></div>
                <div class="ctrl_grid">
                    
                    <div class="img_ctrl grid-item">
                        <div class="title_wrap">
                            <h2 id="cch2">Connect & Capture</h2>
                            <div class="scroll_wrapper">
                                <div class="form__group" style="margin:0.2rem;">
                                    <input id="rob_ip" class="form__input" type="text" name="robot_ip" placeholder="ROBOT IP">
                                    <label for="rob_ip" class="form__label">Robot ip</label>
                                    <button id="rob_conn" class="btn" type="click" value="Subscribe">CONNECT</button>
                                    <div id="led_b" class="led_box">
                                        <div id="rob_led" class="red_led"></div>
                                    </div>
                                </div>
                                <div class = "image_ctrl">
                                    <form id = "camera_settings_form">
                                            <label for="image_resolution" style="margin:0.2rem;">RESOLUTION:</label>
                                            <select class="rob_ctrl_dropdown" name="image_resolution" id="image_resolution">
                                                <option value="13">FRAMESIZE_UXGA (1600 x 1200)</option>
                                                <option value="5">FRAMESIZE_QVGA (320 x 240)</option>
                                                <option value="6">FRAMESIZE_CIF (352 x 288)</option>
                                                <option value="8">FRAMESIZE_VGA (640 x 480)</option>
                                                <option value="9" selected>FRAMESIZE_SVGA (800 x 600)</option>
                                                <option value="10">FRAMESIZE_XGA (1024 x 768)</option>
                                                <option value="12">FRAMESIZE_SXGA (1280 x 1024)</option>
                                            </select>
                                            <label for="quality-slider" style="margin:0.2rem;">QUALITY:</label>
                                            <div class="quality-slider">
                                                <input type="range" min="10" max="63" value="10" class="robot-slider quality-slider" id="video-quality">
                                            </div>
                                            <label for="brightness-slider" style="margin:0.2rem;">BRIGHTNESS:</label>
                                            <div class="brightness-slider">
                                                <input type="range" min="-2" max="2" value="0" class="robot-slider brightness-slider" id="video-brightness">
                                            </div>
                                            <label for="contrast-slider" style="margin:0.2rem;">CONTRAST:</label>
                                            <div class="contrast-slider">
                                                <input type="range" min="-2" max="2" value="0" class="robot-slider contrast-slider" id="video-contrast">
                                            </div>
                                            <label for="saturation-slider" style="margin:0.2rem;">SATURATION:</label>
                                            <div class="saturation-slider">
                                                <input type="range" min="-2" max="2" value="0" class="robot-slider saturation-slider" id="video-saturation">
                                            </div>
                                            <label for="image_special_effect" style="margin:0.2rem;">SPECIAL EFFECT:</label>
                                            <select class="rob_ctrl_dropdown" name="image_special_effect" id="image_special_effect">
                                                <option value="0" selected>No Effect</option>
                                                <option value="1">Negative</option>
                                                <option value="2">Grayscale</option>
                                                <option value="3">Red Tint</option>
                                                <option value="4">Green Tint</option>
                                                <option value="5">Blue Tint</option>
                                                <option value="6">Sepia</option>
                                            </select>
                                            <div class="awb"> 
                                                <label for="awb_switch" class="capture_switch_label">AWB:</label>
                                                <label class="capture_switch" id = "awb_switch">
                                                    <input type="checkbox" id="awb">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="awb_gain"> 
                                            <label for="awb_gain_switch" class="capture_switch_label">AWB GAIN:</label>
                                                <label class="capture_switch" id = "awb_gain_switch">
                                                <input type="checkbox" id = "awb_gain">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <label for="wb_mode" style="margin:0.2rem;">WB MODE:</label>
                                            <select class="rob_ctrl_dropdown" name="wb_mode" id="wb_mode">
                                                <option value="0" selected>Auto</option>
                                                <option value="1">Sunny</option>
                                                <option value="2">Cloudy</option>
                                                <option value="3">Office</option>
                                                <option value="4">Home</option>
                                            </select>
                                            <div class="aec"> 
                                            <label for="aec_switch" class="capture_switch_label">AEC SENSOR:</label>
                                                <label class="capture_switch" id = "aec_switch">
                                                <input type="checkbox" id = "aec">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="aec_dsp"> 
                                            <label for="aec_dsp_switch" class="capture_switch_label">AEC DSP:</label>
                                                <label class="capture_switch" id = "aec_dsp_switch">
                                                <input type="checkbox" id = "aec_dsp">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <label for="aelvl-slider" style="margin:0.2rem;">AE Level:</label>
                                            <div class="aelvl-slider">
                                                <input type="range" min="-2" max="2" value="0" class="robot-slider aelvl-slider" id="video-aelvl">
                                            </div>
                                            <label for="aec-slider" style="margin:0.2rem;">AEC VALUE:</label>
                                            <div class="aec-slider">
                                                <input type="range" min="0" max="1200" value="300" class="robot-slider aec-slider" id="video-aecvalue">
                                            </div>
                                            <div class="agc"> 
                                            <label for="agc_switch" class="capture_switch_label">AGC:</label>
                                                <label class="capture_switch" id = "agc_switch">
                                                <input type="checkbox" id = "agc">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <label for="agc-gain" style="margin:0.2rem;">AGC GAIN:</label>
                                            <div class="agc-gain">
                                                <input type="range" min="0" max="30" value="0" class="robot-slider agc-gain" id="agc-gain">
                                            </div>
                                            <label for="gain-ceiling-slider" style="margin:0.2rem;">GAIN CEILING:</label>
                                            <div class="gain-ceiling-slider">
                                                <input type="range" min="0" max="6" value="3" class="robot-slider gain-ceiling-slider" id="video-gain-ceiling">
                                            </div>
                                            <div class="bpc"> 
                                            <label for="bpc_switch" class="capture_switch_label">BPC:</label>
                                                <label class="capture_switch" id = "bpc_switch">
                                                <input type="checkbox" id = "bpc">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="wpc"> 
                                            <label for="wpc_switch" class="capture_switch_label">WPC:</label>
                                                <label class="capture_switch" id = "wpc_switch">
                                                <input type="checkbox" id = "wpc">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="raw_gma"> 
                                            <label for="raw_gma_switch" class="capture_switch_label">RAW GMA:</label>
                                                <label class="capture_switch" id = raw_gma_switch">
                                                <input type="checkbox" id = "raw_gma">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="lens_cor"> 
                                            <label for="lens_cor_switch" class="capture_switch_label">LENS CORRECTION:</label>
                                                <label class="capture_switch" id = lens_cor_switch">
                                                <input type="checkbox" id = "lens">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="h_mirror"> 
                                            <label for="h_mirror_switch" class="capture_switch_label">H-MIRROR:</label>
                                                <label class="capture_switch" id = h_mirror_switch">
                                                <input type="checkbox" id = "h_mirror">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="v_flip"> 
                                            <label for="v_flip_switch" class="capture_switch_label">V-FLIP:</label>
                                                <label class="capture_switch" id = v_flip_switch">
                                                <input type="checkbox" id = "v_flip">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="dcw"> 
                                            <label for="dcw_switch" class="capture_switch_label">DOWNSIZE:</label>
                                                <label class="capture_switch" id = dcw_switch">
                                                <input type="checkbox" id = "dwc">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                            <div class="colorbar"> 
                                            <label for="colorbar_switch" class="capture_switch_label">COLORBAR:</label>
                                                <label class="capture_switch" id = colorbar_switch">
                                                <input type="checkbox" id = "colorbar">
                                                    <span class="switch_slider round"></span>
                                                </label>
                                            </div>
                                    </form>
                                    <button id="set_image" class="set_image btn" style="margin: 0.4rem 0.2rem 0;width: 98%;">SET CAMERA SETTINGS</button>
                                        
                                        <div class="form__group" style="margin: 0.4rem 0.2rem 0;">
                                            <input id="img_t" class="form__input" type="text" name="img_period" placeholder="Image Period">
                                            <label for="img_t" class="form__label">Fetch period [s]</label>
                                            <button id="img_t_set" class="btn"  style="width: 3rem" type="click" value="Set">SET</button>
                                            <button id="stop_img" class="btn" style="width: 3rem" type="click">STOP</button>
                                            <p></p>   
                                        </div> 
                                    <button id="fetch_img" class="btn" style="margin: 0.4rem 0.2rem 0;width: 98%;">FETCH SINGLE IMAGE</button>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="rob_ctrl grid-item">
                        <div class="title_wrap">
                            <h2 id="rch2">Robot Control</h2>
                            <div class="scroll_wrapper">
                                <div class="arrow_box">
                                    <div id="control-circle"></div>
                                    <button class="left-btn btn robo-movement-btns">&#8592;</button>
                                    <button class="up-btn btn robo-movement-btns">&#8593;</button>
                                    <button class="right-btn btn robo-movement-btns">&#8594;</button>
                                    <button class="down-btn btn robo-movement-btns">&#8595;</button>
                                </div>
                                <div class="per-controls">
                                    <p style="margin: 0.4rem 0 0;">Motor speed:</p>
                                    <div class="speed-slider">
                                        <input type="range" min="0" max="255" value="200" class="robot-slider motor-slider" id="motor-speed">
                                    </div>
                                    <button class="flash btn" id="flashButton">💡</button>
                                    <p style="margin: 0.4rem 0 0;">LED intensity:</p>
                                    <div class="intensity-slider">
                                        <input type="range" min="1" max="9" value="6" class="robot-slider flash-slider" id="flash-intensity">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <button id="close" class="btn">CLOSE</button>
                </div>
            </div>
        </div>
        `;
    popupWin.append(content);
    popupWin.append(overlay);
    renderHook.append(popupWin);
    body.classList.add("popup__open");
    // event listeners
    const flasButton = content.querySelector('.flash')
    flasButton.addEventListener('click', () => robControl.flashLight())
    // event listeners for movement buttons
    const fwButton = content.querySelector('.up-btn');
    const dwButton = content.querySelector('.down-btn');
    const leftButton = content.querySelector('.left-btn');
    const rightButton = content.querySelector('.right-btn');
    // event listeners
    fwButton.addEventListener('click', () => robControl.sendMotorData(1));
    dwButton.addEventListener('click', () => robControl.sendMotorData(2));
    leftButton.addEventListener('click', () => robControl.sendMotorData(3));
    rightButton.addEventListener('click', () => robControl.sendMotorData(4));
    // select set button
    const videoSetButton = content.querySelector('.set_image');
    videoSetButton.addEventListener('click', () => robControl.setCamera());

    return [popupWin, content, body]
}


/* ADD/DELETE NODE POPUP WINDOW */

/**
 * Renders Add node popup window.
 * @returns {Array}     [popupWin, content, body] [Popup window MTML element, popup window content, application body]
 */
export function renderAddNodePage() {
    const body = document.querySelector('body');
    const renderHook = body.getElementsByClassName('grid-container')[0];
    const popupWin = document.createElement('div');
    const overlay = document.createElement('div');
    const content = document.createElement('div');
    popupWin.className = "add_node_win";
    popupWin.id = 'popup_win';
    overlay.className = "node_overlay"
    content.className = "add_node_content";
    content.innerHTML = `
        <h3>Add Node</h3>
        <div class="add_options">
            <button id="create_op" class="btn_option active_op">Create new</button>
            <button id="add_op" class="btn_option">Add Existing</button>
        </div>
        `;
    const createForm = createNodeFormCreator();
    content.append(createForm)
    // addSpinLoader(content);
    popupWin.append(content);
    popupWin.append(overlay);
    renderHook.append(popupWin);
    body.classList.add("popup__open");
    
    return [popupWin, content, body]
}

/**
 * Renders Delete node popup window.
 * @returns     [popupWin, content, body] [Popup window MTML element, popup window content, application body]
 */
export function renderDeleteNodePage() {
    const body = document.querySelector('body');
    const renderHook = body.getElementsByClassName('grid-container')[0];
    const popupWin = document.createElement('div');
    const overlay = document.createElement('div');
    const content = document.createElement('div');
    popupWin.className = "del_node_win";
    popupWin.id = 'popup_win';
    overlay.className = "node_overlay"
    content.className = "add_node_content";
    content.innerHTML = `
        <h3>Delete   Node</h3>
        `;
    const createForm = deleteNodeFormCreator();

    content.append(createForm)
    popupWin.append(content);
    popupWin.append(overlay);
    renderHook.append(popupWin);
    body.classList.add("popup__open");
    
    return [popupWin, content]
}

/**
 * Renders the Add node form element.
 * @returns {HTMLElement}       HTML form element
 */
export function addNodeFormCreator(){
    const addExistForm = document.createElement('form');
    addExistForm.id = 'add_existing_node_form';
    addExistForm.innerHTML = `
        <div class="form__group">
            <input id="exist-name" class="form__input" type="text" placeholder="Name" required>
            <label for="exist-name" class="form__label">Name</label>
        </div>
        <div class="form__group">
            <input id="exist-deveui" class="form__input" type="text" placeholder="Dev EUI" required>
            <label for="exist-deveui" class="form__label">Dev EUI</label>
        </div>
        <div class="form__group">
            <button id="exist_cancel" class="btn" type="button">Cancel</button>
            <button id="exist-add" class="btn" type="submit" value="add">Add</button>
        </div>
        `;
    return addExistForm
}

/**
 * Renders the Create node form element.
 * @returns {HTMLElement}       HTML form element
 */
export function createNodeFormCreator() {
    const createForm = document.createElement('form');
    createForm.id = 'create_node_form';
    createForm.innerHTML = `
            <div class="form__group">
                <input id="n-name" class="form__input" type="text" placeholder="Name" required>
                <label for="n-name" class="form__label">Name</label>
            </div>
            <div class="form__group">
                <input id="n-app_id" class="form__input" type="text" placeholder="Application ID" required>
                <label for="n-app_id" class="form__label">Application ID</label>
            </div>
            <div class="form__group">
                <input id="n-desc" class="form__input" type="text" placeholder="Description" required>
                <label for="n-desc" class="form__label">Description</label>
            </div>
            <div class="form__group">
                <input id="n-deveui" class="form__input" type="text" placeholder="DevEUI" required>
                <label for="n-deveui" class="form__label">Dev EUI</label>
            </div>
            <div class="form__group">
                <input id="n-profid" class="form__input" type="text" placeholder="Device Profile ID" required>
                <label for="n-profid" class="form__label">Device Profile ID</label>
            </div>
            <div class="form__group">
                <input id="n-alt" class="form__input" type="text" placeholder="Reference Altitude" required>
                <label for="n-alt" class="form__label">Reference Altitude</label>
            </div>
            <div class="conf_param">
                <input id="n-fcnt" type="checkbox" placeholder="Skip Message Count" value="true">
                <label for="n-fcnt">Skip Message Count</label>
            </div>
            <div class="form__group">
                <input id="n-tag" class="form__input" type="text" placeholder="Tags">
                <label for="n-tag" class="form__label">Tags</label>
            </div>
            <div class="form__group">
                <input id="n-var" class="form__input" type="text" placeholder="Variables">
                <label for="n-var" class="form__label">Variables</label>
            </div>
            <div class="form__group">
                <input id="n-appKey" class="form__input" type="text" placeholder="AppKey" required>
                <label for="n-appKey" class="form__label">App Key</label>
            </div>
            <div class="form__group">
                <input id="n-nwkKey" class="form__input" type="text" placeholder="nwk Key" required>
                <label for="n-nwkKey" class="form__label">nwk Key</label>
            </div>
            <div class="form__group">
                <button id="n_cancel" class="btn" type="button">Cancel</button>
                <button id="n-create" class="btn" type="submit">Create</button>
            </div>
        `;
    return createForm;
}

/**
 * Renders the Delete node form element.
 * @returns {HTMLElement}       HTML form element
 */
export function deleteNodeFormCreator(){
    const deleteNodeForm = document.createElement('form');
    deleteNodeForm.id = 'delete_node_form';
    deleteNodeForm.innerHTML = `
        <div class="form__group">
            <input id="exist-deveui" class="form__input" type="text" placeholder="Dev EUI" required>
            <label for="exist-deveui" class="form__label">Dev EUI</label>
        </div>
        <div class="form__group">
            <button id="del_cancel" class="btn" type="button">Cancel</button>
            <button id="del-del" class="btn" type="submit" value="add">Delete</button>
        </div>
        `;
    return deleteNodeForm
}

/**
 * Closes a SPA popwindow.
 * @param {HTMLElement} popupWin        Popup window element to be closed
 * @param {HTMLElement} previousPage    Page to be rendered after popup closed
 */
export function closePopUp(popupWin, previousPage) {
        // renderModule.removeSpinLoader();
        popupWin.remove();
        // this.renderConfigPage();
        document.querySelector('body').classList.remove('popup__open');
        activatePageElement(previousPage);
        // body.classList.remove('popup__open');
}


/* DRAG AND DROP FUNCTIONALITY */

/**
 * Initiates drag and drop listeners.
 */
export function updateDragListeners(){
    const nodes = document.querySelectorAll(".node");
    const drop_areas = document.querySelectorAll(".drop_zone");
    nodes.forEach((node) => {
        node.addEventListener("dragstart", dragStart);
        node.addEventListener("dragend", dragEnd);
    });
    drop_areas.forEach((area) =>{
        area.addEventListener("dragover", dragOver);
        area.addEventListener("dragenter", dragEnter);
        area.addEventListener("dragleave", dragLeave);
        area.addEventListener("drop", dragDrop);
    });
}

/**
 * Handler of start drag event.
 */
function dragStart(){
    draggable_node = this;
}

/**
 * Handler of end drag event.
 */
function dragEnd(){
    draggable_node = null;
}

/**
 * Handler of drag over event.
 * @param {Event} e     Drag event object
 */
function dragOver(e){
    e.preventDefault();
}

/**
 * Handler of drag element enter area event.
 */
function dragEnter(){
    this.style.border = "1px dashed #ccc";
    this.parentElement.style.overflow = "unset";
}

/**
 * Handler of drag element leaves area event.
 */
function dragLeave(){
    this.style.border = "none";
}

/**
 * Handler of drag element drop into area event.
 */
function dragDrop(){
    this.style.border = "none";
    this.parentElement.style.overflow = "auto";
    this.appendChild(draggable_node);
}


/* HELPER FUNCTIONS */

/**
 * Activates cliced navigation option.
 * @param {HTMLElement} el  Navigation element
 */
export function activatePageElement(el) {
    const nav = document.querySelectorAll('nav button');
    for  (let btn of nav){
        btn.classList.remove('nav_link__active');
    }
    el.classList.add('nav_link__active');
}

/**
 * Gets the active navigation element from the DOM.
 * @returns {HTMLElement}       Active navigation option
 */
export function getActivePageElement() {
    return document.getElementsByClassName('nav_link__active')[0];
}

/**
 * Changes configuration page layout based on user interactions.
 * @param {HTMLElement} el      Element to be changed 
 * @param {className} cl        CSS class that embraces the changes
 */
export function changeConfigLayout(el, cl){
    el.className = 'grid-item ' + cl;
}

/**
 * System trace function. Creates a paragraph in LOG element with msg.
 * @param {String} msg      Message to be logged
 */
export function writeLog(msg){
    const logs = document.querySelector(".logs");
    let d = new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit', second:'2-digit'});
    msg = "<p>" + d + ": " + msg + "</p>"; 
    logs.innerHTML += msg;
    logs.scrollTop = logs.scrollHeight; // keep scrollbar at the bottom
}

/**
 * Renders a spin loader if application requests it.
 * @param {HTMLElement} renderHook      Element to render the spinner 
 * @param {*} spinText                  Text under the spinner
 */
export function addSpinLoader(renderHook, spinText){
    const spinner = document.createElement('div');
    // const text = document.createElement('p');
    spinner.id = "spinner";
    spinner.className = "spin_overlay";
    spinner.innerHTML = `
        <div class="spin_box">
            <div class="load_spinner"></div>
            <p>${spinText}</p>
        </div>
        `;
    renderHook.append(spinner);
}

/**
 * Removes the spin loader if application requests it.
 */
export function removeSpinLoader(){
    const spinner = document.getElementById('spinner');
    if (spinner != null) {
        spinner.remove();
    }
}
