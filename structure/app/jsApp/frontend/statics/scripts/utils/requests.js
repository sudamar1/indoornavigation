/**
 * This file implements project's custom HTTP requests.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


/**
 * Sends POST request.
 * @param {String}  url         Address to send request
 * @param {Any}     data        Data to be sent
 * @param {JSON}    headers     Headers required by the server
 * @param {Boolean} asynchron   Asynchonous request flag
 * @returns {Promise}           Promise returning HTTP response if resolved
 */
export function sendPostRequest(url, data, headers={}, asynchron=true) {
    const promise = new Promise((resolve, reject) => {
        const req = httpRequest("POST", url, headers, asynchron);
        req.onload = function() {
            resolve(req);
        };
        req.send(data);
    });

    return promise;
}

/**
 * Sends GET request.
 * @param {String}  url         Address to send request
 * @param {JSON}    headers     Headers required by the server
 * @param {Boolean} asynchron   Asynchonous request flag
 * @returns {Promise}           Promise returning HTTP response if resolved
 */
export function sendGetRequest(url, headers={}, asynchron=true) {
    const promise = new Promise((resolve, reject) => {
        const req = httpRequest("GET", url, headers, asynchron);
        req.onload = function() {
            resolve(req);
        }
        req.send();
    });
    return promise;
}

/**
 * XMLHttpRequest() wrapper returning a constructed request object.
 * @param {String}  method              Request type (POST, GET,...)
 * @param {String}  url                 Adress to send request
 * @param {JSON}    headers             Headers required by the server
 * @param {Boolean} asynchron           Asynchronous request flag
 * @returns {XMLHttpRequest.request}    Request object
 */
function httpRequest(method, url, headers, asynchron) {
    let request = new XMLHttpRequest();
    request.open(method, url, asynchron);
    request = setHeaders(request, headers);
    return request;
}

/**
 * Appends headers to the request object.
 * @param {XMLHttpRequest.request} request  Request object to be processed
 * @param {JSON}                   headers  Headers to be used in the request
 * @returns 
 */
function setHeaders(request, headers) {
    Object.entries(headers).forEach(([key, value]) => {
        request.setRequestHeader(key, value);
    })
    return request;
}