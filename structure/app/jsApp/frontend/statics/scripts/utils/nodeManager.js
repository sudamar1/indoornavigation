/**
 * This file implements system node management.
 *
 * It implements ADD/CREATE/DELETE node post requests and response handlers.
 * It implements node fetching from Back End database.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


import {writeLog, removeSpinLoader, closePopUp, renderAvailableNodes} from './render.js'
import {sendPostRequest} from './requests.js'

/**
 * Fetches system nodes from Back End.
 * @returns {Array}     List of node objects
 */
export async function nodes(){
    let data;
    await fetch('/nodes')
        .then(response => data = response.json());
    return data
}

/**
 * Sends a ADD NODE post requests and handles the response.
 * @param {HTMLElement} previousPage  Navigation tag of page prior to window popup
 */
export async function postAddNode(previousPage) {
    const [resp, deveui] = await postNodeRequest("Add", "/nodes/add", "#add_existing_node_form", previousPage)
    writeLog(`Node ${deveui} added to the database: ${(resp["d_save_status"] == 200 ) ? true : `${resp["d_save_status"]}: ${resp["d_content"]["error"]}`}`);
}

/**
 * Sends a CREATE NODE post requests and handles the response.
 * @param {HTMLElement} previousPage  Navigation tag of page prior to window popup
 */
export async function postCreateNode(previousPage) {
    const [resp, deveui] = await postNodeRequest("Create", "/nodes/create", "#create_node_form", previousPage)
    writeLog(`New  ${deveui} node created in the database: ${(resp["d_save_status"] == 200 ) ? true : `${resp["d_save_status"]}: ${resp["d_content"]}`}`)
    writeLog(`New ${deveui} node created in the gateway: ${(resp["g_save_status"] == 200 ) ? true : 
        `${resp["g_save_status"]}: Creation: ${resp["g_content"]["error"]} Keys: ${resp["g_content_keys"]["error"]}`}`)
}

/**
 * Sends a DELETE NODE post requests and handles the response.
 * @param {HTMLElement} previousPage  Navigation tag of page prior to window popup
 */
export async function postDeleteNode(previousPage) {
    const [resp, deveui] = await postNodeRequest("Delete", "/nodes/delete", "#delete_node_form", previousPage)
    console.log(resp)
    writeLog(`Node ${deveui} deleted from the database: ${(resp["d_del_status"] == 200 ) ? true: `${resp["d_del_status"]}: ${resp["d_content"]}`}`)
    writeLog(`Node ${deveui} deleted from the gateway: ${(resp["g_del_status"] == 200 ) ? true : `${resp["g_del_status"]}: ${resp["g_content"]["error"]}`}`)
}


/* HELPER FUNCTIONS */

/**
 * Creates a node post request.
 * @param {String}      action           Action name to be logged
 * @param {String}      url              Back End url to send the request
 * @param {String}      form             Form to collect the data from
 * @param {HTMLElement} previousPage     Navigation tag of page prior to window popup
 * @returns 
 */
async function postNodeRequest(action, url, form, previousPage) {
    writeLog(`Creating "${action.toUpperCase()} NODE" POST request...`);
    writeLog('Processing data...');
    const data = extractNodeData(form)
    const deveui = data['deveui']
    writeLog('Data collected.');
    const headers = {'Content-type': 'application/json'}

    writeLog('Sending the request...');
    const req = await sendPostRequest(url, JSON.stringify(data), headers);

    writeLog(`Request status: ${req.status} ${req.statusText}`)
    removeSpinLoader();
    const resp = JSON.parse(req.response)
    console.log(resp)
    const popupWin = document.getElementById('popup_win');
    if (popupWin){
        closePopUp(popupWin, previousPage);
        renderAvailableNodes(document.getElementById("online_nodes_zone"), await nodes());
    }
    return [resp, deveui]
}

/**
 * Extracts data from the node management form
 * @param {HTMLElement} form    Form to collect the data from
 * @returns {JSON}              Structured data
 */
function extractNodeData(form) {
    const createForm = document.querySelector(form);
    let data = {};
    Array.from(createForm.elements).forEach(input => {
        if (input.nodeName == "INPUT") {
            data[input.id.split("-")[1]] = input.value;
        }
    });
    console.log(data)
    return data;
}