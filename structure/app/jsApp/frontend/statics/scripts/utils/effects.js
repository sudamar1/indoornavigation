/**
 * This file implements Front End dynamics hacks.
 *
 * It implements sidebar/burger menu handler, resize listener, page loaded listener and connection LED status.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


/**
 * If page loaded it removes a preload HTML element to prevent page lags on initialization.
 */
export function pageLoadedListener() {
    /* AFTER LOAD PROCEDURE */
    window.addEventListener("load", () => {
        document.body.classList.remove("preload");
        add_remove_sidebar();
        // renderUtil.renderConfigurationPage();
    });
}

/**
 * Cancels a resize lag of page.
 */
export function resizeListener() {
    /* RESIZE LAG TRICK */
    window.addEventListener("resize", () => {
        let resizeTimer
        document.body.classList.add("resize");
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(() => {
        document.body.classList.remove("resize");
        }, 400);
        add_remove_sidebar();
    });
}

/**
 * Shows burger menu icon if application size under a threshold.
 */
export function burgerMenuListeners(){
    /* SIDEBAR MOVEMENT */
    document.addEventListener("DOMContentLoaded", () => {
        const nav = document.querySelector(".nav");
        // show menu if burger clicked
        document.querySelector(".burger_btn").addEventListener("click", () => {
            nav.classList.add("nav__open");
        });
        // hide menu if overlay clicked
        document.querySelector(".nav_overlay").addEventListener("click", () => {
            nav.classList.remove("nav__open");
        });
        // hide menu if option selected
        document.querySelectorAll(".nav_link").forEach(link => {
            link.addEventListener("click", () => {
                nav.classList.remove("nav__open");
            });
        });
    });
}

/**
 * Adds/Removes sidebar based on window size. Works alongside burgerMenuListeners().
 */
function add_remove_sidebar() {
    if (window.innerWidth < 700) {
        const sidebar = document.querySelector("nav");
        sidebar.classList.remove('grid-item');
        sidebar.classList.remove('grid-item-sidebar');
    }
    else {
        const sidebar = document.querySelector("nav");
        sidebar.classList.add('grid-item');
        sidebar.classList.add('grid-item-sidebar');
    }
}

/**
 * Connection LED handler, if MQTT connected LED = green, red otherwise.
 * @returns {null}
 */
export function connectionLedStatus() {
    /* CONNECTION LED */
    const led_box = document.getElementById('led_b');
    console.log(window.isConnected);
    if (window.isConnected == 0){
        led_box.firstElementChild.className = 'red_led';
        led_box.querySelector('p').innerHTML = 'Disconnected'
        return;
    }
    led_box.firstElementChild.className = 'green_led';
    led_box.querySelector('p').innerHTML = 'Connected'
}
