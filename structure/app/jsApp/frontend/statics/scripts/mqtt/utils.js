/**
 * This file implements core MQTT communication utilities utilizing PAHO library from Eclipse Foundation.
 *
 * It implements connection, topic subscription and publishing, handles system configuration data to be MQTT complient.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


import * as mqttCallbacks from './callbacks.js';
import {writeLog} from '../utils/render.js';


/**
 * Connects Front End to a MQTT Broker. (Gateway)
 * @param {Event} evt                Submition event object
 * @returns {Paho.MQTT.Client}       PAHO MQTT client
 */
export function connect(evt)
{   
    evt.preventDefault();  // Prevent from default submit action trigger (e.g., reloads page)
    
    // Form collection
    const connectionForm = document.getElementById('connect_form')
    let server = connectionForm.elements.server.value;
    let port = +(connectionForm.elements.port.value);    
    let cleanSessions = connectionForm.elements.cl_ses.checked;
    let username = connectionForm.elements.username.value;
    let password = connectionForm.elements.password.value;
    let options = {
        timeout: 3,
        cleanSession: cleanSessions,
        onSuccess: mqttCallbacks.onConnect,
        onFailure: mqttCallbacks.onFailure,
        };

    if (checkConnectionInputs(server, port, username, password)){
        console.log(`Connecting to: ${server}:${port}, clean session: ${cleanSessions}`);
        writeLog(`Connecting to broker ${server}:${port} ...`);
        let mqttClient = new Paho.MQTT.Client(server, port, "INAVIAPP");    
        // CONNECTION CALLBACKS
        mqttClient.onConnectionLost = mqttCallbacks.onConnectionLost;
        mqttClient.onMessageArrived = mqttCallbacks.onMessageArrived;
        mqttClient.onConnected = mqttCallbacks.onConnect;
        // CONNET TO BROKER
        mqttClient.connect(options);
        return mqttClient;
    }
}

/**
 * Subscribe an arbitrary MQTT topic.
 * @returns {Boolean, JSON}     Json containing sub. topic and options, false otherwise
 */
export function subscribeTestMqttTopic()
{
    let failMessage = "Subscribtion faild, no MQTT broker connected!";
    if (isConnected(failMessage)) {
        let sub_form = document.querySelector("#form_test_subscribe");
        let subscribeTopic = sub_form.elements.sub_topic.value;
        let subscribeQos = +(sub_form.elements.sub_qos.value);
        if (subscribeQos > 2) {
            subscribeQos = 0;        
        }
        let subscribeOptions={
            qos: subscribeQos,
            };
        return {"topic": subscribeTopic, "options": subscribeOptions};
    }
    return false;
}

/**
 * Publish an arbitrary MQTT topic.
 * @returns {Boolean, JSON}     Json containing pub. topic, message and options, false otherwise
 */
export function publishTestMqttMessage()
{
    let failMessage = "Publish faild, no MQTT broker connected!";
    if (isConnected(failMessage)) {
        let pub_form = document.querySelector("#form_test_publish");
        let msg = pub_form.elements.pub_msg.value;
        let topic = pub_form.elements.pub_topic.value;
        let publishQos = +(pub_form.elements.pub_qos.value);
        let retain = pub_form.elements.pub_retain.checked;

        if (publishQos > 2) {
            publishQos = 0;
            console.log("Invalid QoS input (0<=QoS>=2). QoS was set to 0.");
            writeLog("Invalid QoS input (0<=QoS>=2). QoS was set to 0.");
        }
        console.log(`Topic: ${topic}, Qos: ${publishQos}, Retain: ${retain}, Message: ${msg}`);
        writeLog(`Publish: ${topic} => "${msg}"`);
        writeLog("Sending the message ...");
        
        let message = new Paho.MQTT.Message(msg);
        if (topic=="") {
            message.destinationName = "defaultTopic";
        } else {
            message.destinationName = topic;
        }
        message.qos = publishQos;
        message.retained= retain;
        return message;
    }
    return false;
}

/**
 * Creates system configuration messages or message in case of single-node config. based on Front End data from user.
 * @param {JSON} nodes              Nodes objects available in the system.
 * @returns {Array, Boolean}        [messages, topics] if process successful, false otherwise
 */
export function sendConfigurationData(nodes)
{
    let failMessage = "Sending faild, no MQTT broker connected";
    if (isConnected(failMessage)){
        let nodesToConfig = getNodesToConfigure();
        // console.log(nodesToConfig.length);
        if (nodesToConfig.length > 0) {
            let configDiv = document.getElementById('config').lastChild;  // TODO: arbitrary modudulation by id or so
            let messages = {};
            const [modulationType, configForm] = getModulationType(configDiv);
            if (!modulationType) {return false;}
            
            let [configurationData, qos] = extractConfigurationData(modulationType, configForm);
            if (nodesToConfig.length > 1) { // multi-node configuration
                let multiNodeData = extractMultiNodeConfiguration(modulationType, configForm);
                // map configs
                messages = getMessagesMultiNodesConfig(nodes, nodesToConfig, configurationData, multiNodeData, qos)
            } else { // single-node configuration
                console.log(nodes[nodesToConfig[0]])
                let message = getConfigurationMessage(modulationType, configurationData, qos, nodes[nodesToConfig[0]].tx);
                if (!message){return false;} // validation check
                messages[nodesToConfig[0]] = message;
            }
            writeLog(`${Object.keys(messages).length} MQTT Messages created`)
            const topics = getAckTopics(nodesToConfig, nodes);
            return [messages, topics];
        }
        console.log("No nodes to configure drop available node to the configuration");
        writeLog("No nodes to configure drop available node to the configuration");
        return false; 
    }
    console.log("FAIL: Configuration failed! Data empty! Please check broker connection");
    return false;
}


/* HELPER FUNCTIONS */

/**
 * Subscribe configuration ack topics to acknowledge the proper configuration
 * @param {Array} nodesToConfig     Nodes to be configured
 * @param {Array} nodes             Nodes available in the system
 * @returns {JSON}                  Subscribed topics which will be listened for ACK
 */
function getAckTopics(nodesToConfig, nodes) {
    let topics = {};
    nodesToConfig.forEach( n => {
        const subscribeTopic = nodes[n].txack;
        topics[n] = {"topic": subscribeTopic, "options": {qos: 0}};
    });

    return topics
}

/**
 * Validation of the connection form.
 * @param {String} server        MQTT Broker IP
 * @param {int}    port          MQTT Broker WebSocket port (e.g., 9001)
 * @param {String} username      MQTT Broker authentication username
 * @param {String} password      MQTT Broker authentication password
 * @returns {Boolean}
 */
function checkConnectionInputs(server, port, username, password){
    if (server == "" || port == "") {
        writeLog("IP address and port is mandatory for the connection process! Please fill it correctly");
        return false;
    }
    if (username != "" && password != "") {
        options.userName = username;
        options.password = password;
    }
    return true
}

/**
 * Creates BufferArray from JSON.
 * @param {JSON} message       Message to be transfered to a BufferArray interpretation
 * @returns {bufferArray}      Message in BufferArray interpretation
 */
function jsonToBufferArray(message) {
    let tempStr = JSON.stringify(message, null, 0);
    let bufferArray = new Uint8Array(tempStr.length);
    for (var i = 0; i < tempStr.length; i++) {
      bufferArray[i] = tempStr.charCodeAt(i);
    }
    return bufferArray;
}

/**
 * Connection status handler.
 * @param {String} message  Message to be logged
 * @returns {Boolean}       True if connected, False otherwise
 */
function isConnected(message){
    if (window.isConnected == 0) {
        writeLog(message);
        return false;
    }
    return true;
}

/**
 * Extracts data from Front End upon submission.
 * @param {String}      modulationType  Modulation type
 * @param {HTMLElement} configForm      Front End configuration form
 * @returns {Array}                     [data, qos] JSON with configuration data and MQTT qos
 */
function extractConfigurationData(modulationType, configForm){
    let configurationData;
    let qos;

    if (modulationType == "pwm") {
        let frequency = +(configForm.elements.pwm_f.value);
        let dutyCycle = +(configForm.elements.pwm_d.value);
        qos = +(configForm.elements.pwm_qos.value);
        configurationData = {
            frequency: frequency,
            modulationType: modulationType,
            dutyCycle: dutyCycle,
        };
    } else if (modulationType == "sin") {
        let frequency = +(configForm.elements.sin_f.value);
        let amplitude = +(configForm.elements.sin_a.value);
        let phase = +(configForm.elements.sin_p.value);
        qos = +(configForm.elements.sin_qos.value);
        configurationData = {
                frequency: frequency,
                modulationType: modulationType,
                amplitude: amplitude,
                phase: phase
            };
    }
    
    return [configurationData, qos];
}

/**
 * Extracts data from Front End with multi-node configuration.
 * @param {String}      modulationType  Modulation type
 * @param {HTMLElement} configForm      Form to extract data from
 * @returns {JSON}                      Contains individual configuration for each node (eui: configuration)
 */
function extractMultiNodeConfiguration(modulationType, configForm){
    let fix;
    // const confForm = document.getElementById("form_configuration_step");
    if (modulationType == 'sin') {
        fix = {
            "f_fix": configForm.elements.sin_f_fix.checked,
            "a_fix": configForm.elements.sin_a_fix.checked,
            "p_fix": configForm.elements.sin_p_fix.checked,
            };
    } else if (modulationType == 'pwm') {
        fix = {
            "f_fix": configForm.elements.pwm_f_fix.checked,
            "d_fix": configForm.elements.pwm_d_fix.checked,
            };
    } else {
        return false;
    }
    // steps = {f_step: int, a_step: int,}
    return mapStepWithParam(configForm, fix, modulationType);
}

/**
 * Extracts parameter offsets from initial value and maps them to a step JSON for further processing.
 * @param {HTMLElement} confForm         Configuration form to get the steps from
 * @param {JSON}        fix              Info about fix parameters
 * @param {String}      modulationType   Modulation type
 * @returns {JSON}                      
 */
function mapStepWithParam(confForm, fix, modulationType){
    let steps = {}
    let cnt = 1;
    for (let key in fix){
        let identifier = key.charAt(0);
        
        if (fix[key]){
            steps[`${identifier}_step`] = 0;
            cnt ++;
        } else {
            steps[`${identifier}_step`] = parseInt(confForm[`${modulationType}_${identifier}_step`].value);
            
        }
    }
    if (cnt > Object.keys(fix).length){
        writeLog("No variable parameter. Please uncheck at least one fix parameter");
        return false;
    }
    return steps;
}

/**
 * Validates the configuration data before the further processing
 * @param {String}  modulationType      Modulation type
 * @param {JSON}    configurationData   Configuration data 
 * @param {Int}     qos                 MQTT quality of service
 * @returns {Boolean}                   True if success, False format violation
 */
function validateConfigurationData(modulationType, configurationData, qos) {
    if (modulationType == 'pwm' && (configurationData.dutyCycle < 0 || configurationData.dutyCycle > 100)) {
        console.log("Invalid Duty Cycle input (0<DC<100). Message couldn't be sent.");
        writeLog("Invalid Duty Cycle input (0<DC<100). Message couldn't be sent.");
        
        return false;
    } 
    if (qos > 2) {
        console.log("Invalid QoS input (0<=QoS<=2). QoS was set to 0.")
        writeLog("Invalid QoS input (0<=QoS<=2). QoS was set to 0.")
        return false;
    }
    return true;
}

/**
 * Get list of nodes to be configured
 * @returns {Array}     Nodes to be configured
 */
function getNodesToConfigure(){
    let tmp = document.getElementById("configure_nodes").querySelectorAll('button');
    let nodesToConfig = [];
    tmp.forEach(element => {  // get only node ids
        nodesToConfig.push(element.id);
    });
    // console.log(nodesToConfig);
    return nodesToConfig;
}

/**
 * Extracts modulation type from Front End.
 * @param {HTMLElement} config_div  Div to extract the modulation type from
 * @returns {Array}                 [modulation type, HTML element of the configuration form]
 */
function getModulationType(configDiv) {
    let modulationType;
    let configForm = configDiv.querySelector("form");
    const configDivId = configDiv.id;
    if (configDivId == 'sin'){
        modulationType = "sin";
        // config_form = configDivId.querySelector("#form_configuration_param");
    } else if (configDivId == 'pwm') {
        modulationType = "pwm";
        // config_form = configDivId.querySelector("#form_configuration_param");
    } else {
        console.log("different configuration than SIN")
        return false;
    }
    return [modulationType, configForm];
    }

/**
 * Creates a multi-node configuration messages based on extracted steps of modulation parameters.
 * 
 * @param {JSON} nodes          JSON with nodes objects
 * @param {Array} nodesToConfig List of node names (Ids).
 * @param {Object} data         Configuration of first node, other configuration will be derived.
 * @param {Object} steps        Steps of modulation parameters from first node configuration.
 * @param {Int} qos             QoS for MQTT protocol.
 * @return {Array}              Holds Paho.MQTT.Message objects. 
 */
function getMessagesMultiNodesConfig(nodes, nodesToConfig, data, steps, qos) {
    if (!steps){return false;}
    let messages = [];
    let cnt = 0;
    for (let nodeName of nodesToConfig) {
        let node = {};
        for (let dataKey in data) {
            if (dataKey != "modulationType"){
                // console.log("step " + steps[`${dataKey.charAt(0)}_step`]);
                node[dataKey] = data[dataKey] + (steps[`${dataKey.charAt(0)}_step`]*cnt);
            }
        }
        // console.log(nodes[nodeName].tx);
        node['modulationType'] = data.modulationType;
        // messages.push(getConfigurationMessage(data.modulationType, node, qos, nodes[nodeName].tx))
        messages[nodeName] = getConfigurationMessage(data.modulationType, node, qos, nodes[nodeName].tx);
        cnt++;
    }
    // console.log(messages);
    return messages;
}

/** 
 * Creates a Paho.MQTT.Message object with configuration message
 * @param {String}  modulationType      Type of light modulation.
 * @param {Object}  configurationData   Crutial modulation parameters in a json object.
 * @param {Int}     qos                 QoS for MQTT protocol.
 * @param {String}  topic               MQTT topic to write the message.
 * 
 * @return {Paho.MQTT.Message}          MQTT message holding INAVI system configurations.
*/
function getConfigurationMessage(modulationType, configurationData, qos, topic) {
    
    if (validateConfigurationData(modulationType, configurationData, qos)){
        let configMessage = {
            confirmed: true,
            fPort: 1,
            object: configurationData
        }
        
        let message = new Paho.MQTT.Message(jsonToBufferArray(configMessage));
        message.destinationName = topic;
        message.qos = qos;
        message.retained= false;  //TODO: add retain functionality
        return message;
    }
    return false;
}
