/**
 * This file implements MQTT callbacks.
 *
 * It implements onConnectionLost, onFailure, onConnect and onMessageArrived.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


import {writeLog} from '../utils/render.js'
import {connectionLedStatus} from '../utils/effects.js'
import { sendPostRequest } from '../utils/requests.js';
import {App} from '../app.js'

const RECONECT_TIME_OUT = 2000; 


/**
 * Callback handler on lost connection.
 * @param {Response} responseObject WebSocket response object 
 */
export function onConnectionLost(responseObject) {
    console.log("Connection Lost: "+responseObject.errorMessage);
    writeLog("Broker disconnected!");
    writeLog("No broker connected");
    window.isConnected=0;
    connectionLedStatus();
}

/**
 * Callback handler on connection failure.
 */
export function onFailure() {
    writeLog("Connection Failed");
}

/**
 * Callback handler on received message.
 * @param {PAHO.MQTT.message} message   Received MQTT message in subscribed topic.
 * @returns {null}
 */
export async function onMessageArrived(message) {
    console.log(message);
    const topicId = message.destinationName.substring(message.destinationName.lastIndexOf('/') + 1);
    if (topicId == "txack") {
        const msg = JSON.parse(message.payloadString)
        writeLog(`${msg["devEUI"]} configured!`)
        // saveLastSuccessfulConfig(msg["devEUI"]);
        console.log(`Received from ${message.destinationName}`);
        document.dispatchEvent(new CustomEvent('txAckReceived', {detail: {topic: message.destinationName, devEUI: msg["devEUI"]}}));
        return;
    }
    writeLog(`Topic ${message.destinationName}`);
    writeLog(`Received message: ${message.payloadString}`);
}

/**
 * Callback handler on connection established.
 */
export function onConnect() {
    window.isConnected = 1;
    writeLog("Connected");
    console.log("Network status: Connected");
    connectionLedStatus();
}

