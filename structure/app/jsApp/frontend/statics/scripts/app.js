/**
 * This file implements the core SPA class. This file covers the whole application's funtionality
 * utilizing our custom modules.
 *
 * It implements application's initialization, rendering and event handlings.
 *
 * @author Martin Suda.
 * @since  22.05.2022
 */
/** jshint */


import * as util from './mqtt/utils.js';
import * as renderModule from './utils/render.js'
import * as nodeMan from './utils/nodeManager.js'
import {
    pageLoadedListener,
    resizeListener,
    burgerMenuListeners} from './utils/effects.js'
import * as RobotApi from './robot/api.js'
import { sendPostRequest } from './utils/requests.js';


/**
 * This class implements the SPA functionality.
 * It covers all application's life-cycles
 *
 * It implements Event handlers, application rendering and Event Listeners.
 *
 * @since      22.05.2022
 * @access     public
 *
 * @constructs namespace.Class
 */
export class App {

    /* STATIC CLASS VARIABLES */
    static mqttBroker;
    static nodes;
    static lastNodeConfig;
    

    /* MQTT METHODS */

    /**
     * MQTT connection handler.
     * @param {Event} event 
     */
    static mqttConnect(event) {
        this.mqttBroker = util.connect(event);
    }

    /**
     * MQTT disconnect handler.
     */
    static mqttDisconnect() {
        if (window.isConnected == 1){
            this.mqttBroker.disconnect();
            console.log("Network status: Disconnected");
        } else {
            renderModule.writeLog("The broker already disconnected.");
        }
    }

    /**
     * Sends fingerprinting configuration to selected nodes.
     * @param {JSON} nodes  Dictionary with available system nodes
     * @returns {Promise}
     */
    static mqttSendConfiguration(nodes) {
        // TODO: find if async necessary -> shouldnt be since it is promise!
        let data = util.sendConfigurationData(nodes);
        let p = Promise.resolve();
        if (data){
            let [messages, topics] = data;
            Object.entries(messages).forEach(([eui, message]) => {
                this.mqttBroker.subscribe(topics[eui]["topic"]);
                console.log(`Subscribed: ${topics[eui]["topic"]}`);

                p = p.then(function () {
                    App.lastNodeConfig[eui] = JSON.parse(message.payloadString)["object"];
                    App.mqttBroker.send(message);
                    // console.log(`Message content:\n ${message.payloadString}`);
                    // console.log("Message sent!");
                    // renderModule.writeLog(`Message content:\t${message.payloadString}`);
                    renderModule.writeLog(`${eui} configuration message sent!`);
                    return new Promise(function (resolve) {
                            setTimeout(resolve, 4000);
                        });
                });
            });
            return;
        }
    }

    /**
     * Subscribes an arbitrary MQTT topic requested by the user.
     * @returns {null}
     */
    static mqttTestSubscribeTopic() {  // TODO: investigate possible data failure
        // TODO: if mqtt broker not connected dont raise error
        let data = util.subscribeTestMqttTopic();
        console.log(data);
        if (data) {
            let topic = data.topic;
            console.log(`Subscribing to topic: ${topic} QoS: ${data.options.qos}`);
            renderModule.writeLog(`Subscribing topic: ${topic}`);
            this.mqttBroker.subscribe(topic, data.options);
            return;
        }
        console.log(`No topic entered`);
        renderModule.writeLog(`No topic to subscribe entered`);
    }

    /**
     * Publishes an arbitrary message to a topic upon user's request.
     * @returns {null}
     */
    static mqttTestPublish(){  // TODO: investigate possible data failure
        let data = util.publishTestMqttMessage();
        if (data){
            this.mqttBroker.send(data);
            renderModule.writeLog(`Message sent!`);
            return;
        }
        console.log(`Fault data`);
        renderModule.writeLog(`Fault data`);
    }
    

    /* RENDER METHODS */

    /**
     * Renders the configuration layout.
     */
    static renderConfigPage(){
        const defaultConf = renderModule.renderConfigurationPage();
        this.addModulationEventListeners();
        this.addSubmitConfigurationListener(defaultConf);
    }
    static renderMqttTestPage(){
        renderModule.renderMqttTestPage();
        document.getElementById('form_test_subscribe').addEventListener('submit', function(e){
            e.preventDefault();
            App.mqttTestSubscribeTopic();
        });
        document.getElementById('form_test_publish').addEventListener('submit', function(e){
            e.preventDefault();
            App.mqttTestPublish();
        });
    }

    /**
     * Renders the node detection popup window.
     */
    static renderDetectNodePage(){
        const prevPage = renderModule.getActivePageElement();
        renderModule.activatePageElement(renderModule.nav_node_detect);
        const [popupWin, content, body] = renderModule.renderDetectNodePage();
        
        document.getElementById('rob_conn').addEventListener('click', () => {
            renderModule.addSpinLoader(content, 'Check Robot connection');
            RobotApi.connect();
            });
        document.getElementById('fetch_img').addEventListener('click', RobotApi.fetchImage);
        document.getElementById('img_t_set').addEventListener('click', RobotApi.setPeriod);
        this.closePopUpListener(document.getElementById('close'), popupWin, prevPage);
        this.closePopUpListener(document.querySelector(".node_overlay"), popupWin, prevPage);
    }

    /**
     * Renders the ADD/CREATE popup window.
     */
    static renderAddNodesPage(){
        const prevPage = renderModule.getActivePageElement();
        renderModule.activatePageElement(renderModule.nav_add_nodes);
        const [popupWin, content, body] = renderModule.renderAddNodePage();
        const createBtn = document.getElementById('create_op');
        const addBtn = document.getElementById('add_op');
        
        createBtn.addEventListener('click', () => {
            addBtn.classList.remove('active_op');
            createBtn.classList.add('active_op'); 
            content.lastElementChild.remove();
            const createForm = renderModule.createNodeFormCreator();
            content.append(createForm);
            content.querySelector('form').addEventListener("submit", (evt) => {
                evt.preventDefault();
                renderModule.addSpinLoader(content, 'Creating new node...');
                console.log(prevPage)
                nodeMan.postCreateNode(prevPage);
            });
            this.closePopUpListener(document.getElementById('n_cancel'), popupWin, prevPage);
        });
        addBtn.addEventListener('click', () => {
            createBtn.classList.remove('active_op');
            addBtn.classList.add('active_op');
            const addExistForm = renderModule.addNodeFormCreator();
            content.lastElementChild.remove();
            content.append(addExistForm); 
            content.querySelector('form').addEventListener("submit", (evt) => {
                evt.preventDefault();
                renderModule.addSpinLoader(content, 'Adding the node to the database...');
                nodeMan.postAddNode(prevPage);
            });
            this.closePopUpListener(document.getElementById('exist_cancel'), popupWin, prevPage);   
        });
        this.closePopUpListener(document.querySelector(".node_overlay"), popupWin, prevPage);
        this.closePopUpListener(document.getElementById('n_cancel'), popupWin, prevPage);
        content.querySelector('form').addEventListener("submit", (evt) => {
            evt.preventDefault();
            renderModule.addSpinLoader(content, 'Creating new node...');
            nodeMan.postCreateNode(prevPage);
        });
    }

    /**
     * Renders the DELETE node popup window.
     */
    static renderDeleteNodesPage() {
        const prevPage = renderModule.getActivePageElement();
        renderModule.activatePageElement(renderModule.nav_del_nodes);
        const [popupWin, content] = renderModule.renderDeleteNodePage();

        this.closePopUpListener(document.querySelector(".node_overlay"), popupWin, prevPage);
        this.closePopUpListener(document.getElementById('del_cancel'), popupWin, prevPage);
        content.querySelector('form').addEventListener("submit", (evt) => {
            evt.preventDefault();
            renderModule.addSpinLoader(content, 'Deleting node...');
            nodeMan.postDeleteNode(prevPage);
        });
    }

    /**
     * Renders docs popup window.
     */
    static renderDocsPage(){
        renderModule.activatePageElement(renderModule.nav_docs);
        alert("NOT IMPLEMENTED YET");
        this.renderConfigPage();
    }

    /**
     * Renders available nodes to the drag and drop area.
     */
    static async renderAvailableNodes(){
        const renderHook = document.getElementById("online_nodes_zone");
        this.nodes = renderModule.renderAvailableNodes(renderHook, await nodeMan.nodes());
    }


    /* LISTENER METHODS */

    /**
     * Creates Event Listener handling the node configuration upon user's request.
     * @param {HTMLElement} domObject   Element containing the configuration form
     */
    static addSubmitConfigurationListener(domObject) {
        const conf_form = domObject.querySelector('form');
        conf_form.addEventListener("submit", event => {
            event.preventDefault();
            this.mqttSendConfiguration(this.nodes)
        });
    }

    /**
     * Creates listeners handling modulation types.
     */
    static addModulationEventListeners() {
        const modulations = document.getElementById('mod_options').querySelectorAll('button');
        const pwm = modulations[0];
        const sin = modulations[1];
        // const custom = modulations[2];
        sin.addEventListener('click', () => {
            renderModule.removePreviusModOption();
            renderModule.sinConfiguration();
            const sin = document.getElementById("config");
            this.addSubmitConfigurationListener(sin);
        });
        pwm.addEventListener('click', () => {
            renderModule.removePreviusModOption();
            renderModule.pwmConfiguration();
            const pwm = document.getElementById("config");
            this.addSubmitConfigurationListener(pwm);
        });
        // custom.addEventListener('click', () => {
        //     renderModule.removePreviusModOption();
        //     renderModule.customConfiguration();
        //     const custom = document.getElementById("config");
        //     this.addSubmitConfigurationListener(custom);
        // });
    }

    /**
     * Attaches an event listener to a element.
     * @param {HTMLElement} el              Element to hold the event listener (e.g., button)
     * @param {HTMLElement} popupWin        Popup window to be closed
     * @param {HTMLElement} previousPage    Element containing the navigation option to render upon window closing
     */
    static closePopUpListener(el, popupWin, previousPage) {
        el.addEventListener("click", () => {
            renderModule.closePopUp(popupWin, previousPage)
        });
    }


    /**
     * Initializes the application when first loaded.
     * It creates all neccessary event listeners and fetches all neccessary data from Back End.
     */
    static init() {
        this.lastNodeConfig = {};
        window.isConnected = 0;
        pageLoadedListener();
        resizeListener();
        burgerMenuListeners();
        this.renderConfigPage();
        this.renderAvailableNodes();
        document.querySelector('#connection').addEventListener("submit", function(e){
            if (window.isConnected == 1){ // TODO: rewrite the isConnected helper func
                e.preventDefault();
                renderModule.writeLog("The MQTT broker has already been connected.");
                return false;
            }
            App.mqttConnect(e);
            console.log(App.mqttBroker);
        });
        document.addEventListener('txAckReceived', (e) => {
            const eui = e.detail.devEUI
            
            this.mqttBroker.unsubscribe(e.detail.topic);
            console.log(`${eui} unsubscribed`)
            const data = JSON.stringify({"devEUI": eui, "config": this.lastNodeConfig[eui]});
            sendPostRequest('nodes/config/save',
                            data,
                            {'Content-Type': 'application/json'}).then(resp => {
                                renderModule.writeLog(`${eui} configuration saved in DB: ${resp.responseText}`);
                            });
            
        });
        document.getElementById('disconnect').addEventListener("click", this.mqttDisconnect.bind(this));
        renderModule.nav_config.addEventListener("click", this.renderConfigPage.bind(this));
        renderModule.nav_node_detect.addEventListener("click", this.renderDetectNodePage.bind(this));
        renderModule.nav_mqtt.addEventListener("click", this.renderMqttTestPage.bind(this));
        renderModule.nav_add_nodes.addEventListener("click", this.renderAddNodesPage.bind(this));
        renderModule.nav_del_nodes.addEventListener("click", this.renderDeleteNodesPage.bind(this));
        // renderModule.nav_docs.addEventListener("click", this.renderDocsPage.bind(this));
    }
}

/* Start the application */
App.init();
