# Setup backend with Docker

1. Download Docker Desktop from official [site](https://docs.docker.com/desktop/).
2. Start docker app (starts the docker engine and interfaces).
3. Clone this repository to your local machine and navigate to the [jsApp](.) directory.
4. Execute the [set_environment.sh](set_environment.sh) shell script.
    - creates directory to store persist mongodb data (./backend/database/mongodbdata/)
    - builds docker images
5. Execute the [start_container.sh](start_container.sh) shell script.
    - starts the backend cointainer (flask + mongodb)
6. Open the app in the browser at:
    ```
    127.0.0.1:8000
    ```
7. Connect to the gateway via MQTT
    ```
    ip: <gw_ip>
    port: 9001
    ```
8. Send a configuration message at the bottom of the app

## Troubleshooting
1. Shell scripts not executable or permission denied error. Run...
   ```
   chmod +x
   # or open git Bash and run
   source <script_name>.sh
   ```
