#!/bin/sh


DIR="./backend/database/mongodbdata"
echo ${DIR}

if [ ! -d "${DIR}" ]; then
    # Take action dir does not exists. #
    echo "Mongodb not initilized on the system. Initilizing..."
    mkdir ${DIR}
else
    echo "Mongodb data ready."
fi

echo "Building docker image..."
docker compose build
