"""This file implemenents node detection and processing class. 

numpy, cv2 libraries required.

Classes:
    * DetectionProcessing - static class comprises processing methods
        * read
        * read_bytes
        * read_bytes_color
        * blur
        * gauss_blur
        * binarization
        * morf_opening
        * morf_closing
        * gradient
        * thresholds
    * DetectionAlgorithms - static class comprises detection methods
        * detect_hough

Author: Martin Suda
Date: 22/05/2022
"""


import cv2 as cv
import numpy as np


class DetectionProcessing:
    """Implements an abstract class with image processing methods.
    """
    def __init__(self) -> None:
        pass

    @staticmethod
    def read(path):
        """Loads an image using the path

        :param path: Image path
        :type path: str
        :return: (Original image ,Image shape ,Image copy)
        :rtype: tuple
        """
        im = cv.imread(path, cv.IMREAD_GRAYSCALE) # reads pic in BGR!!

        return im, np.shape(im), im.copy()

    @staticmethod
    def read_bytes(b):
        """Loads an image from bytes buffer in grayscale

        :param b: bytes buffer 
        :type b: bytes
        :return: (Original image in grayscale, Image shape ,Image copy in gray scale)
        :rtype: tuple
        """
        im = cv.imdecode(np.frombuffer(b, np.uint8), cv.IMREAD_GRAYSCALE) # reads pic in BGR!!
        return im, np.shape(im), im.copy()

    def read_bytes_color(b):
        """Loads an image from bytes buffer as a RGB image

        :param b: bytes buffer 
        :type b: bytes
        :return: (Original image in RGB format, Image shape ,Image copy in grayscale)
        :rtype: tuple 
        """
        imageBGR = cv.imdecode(np.frombuffer(b, np.uint8), cv.IMREAD_COLOR)
        imageRGB = cv.cvtColor(imageBGR , cv.COLOR_BGR2RGB)
        gray = cv.cvtColor(imageBGR, cv.COLOR_BGR2GRAY)

        return imageRGB, np.shape(gray), gray

    @staticmethod
    def blur(im, winx=20, winy=20):
        """Image blur utilizing a normilized box filter

        :param im: Image to blur
        :type im: numpy array
        :param winx: Filter width, defaults to 20
        :type winx: int, optional
        :param winy: Filer height, defaults to 20
        :type winy: int, optional
        :return: Blurred image
        :rtype: numpy array
        """
        return cv.blur(im,(winx,winy)) 

    @staticmethod
    def gauss_blur(im, winx=21, winy=21):
        """Image blur utilizing Gaussian filter

        :param im: Image to blur
        :type im: numpy array
        :param winx: Filter width, defaults to 21
        :type winx: int, optional
        :param winy: Filter height, defaults to 21
        :type winy: int, optional
        :return: Blurred image
        :rtype: numpy array
        """
        return cv.GaussianBlur(im,(winx,winy),10)

    @staticmethod
    def degrade_background(im, th=245, const=0.2):
        """Degrades pixels under the threshold by a factor.

        :param im: Image to degrade
        :type im: numpy array
        :param th: Threshold, defaults to 245
        :type th: int, optional
        :param const: factor of degradation, defaults to 0.2
        :type const: float, optional
        :return: Degraded image
        :rtype: numpy array
        """
        im[im<th] = im[im<th]*const
    
        return im

    @staticmethod
    def binarization(im, th=245):
        """Performs binarization upon a specified threshold

        :param im: Image to binarize
        :type im: numpy array
        :param th: Threshold, defaults to 245
        :type th: int, optional
        :return: Binary image
        :rtype: numpy array
        """
        im[im<th] = 1
        # _, im = cv.threshold(im,th,255,cv.THRESH_BINARY)  # slow

        return im

    @staticmethod
    def morf_opening(im, kernel=cv.getStructuringElement(cv.MORPH_ELLIPSE, (30,30))):
        """Performs morphological opening

        :param im: Image to perform morphological opening
        :type im: numpy array
        :param kernel: Structuring element, defaults to cv.getStructuringElement(cv.MORPH_ELLIPSE, (30,30))
        :type kernel: numpy array, optional
        :return: Processed image
        :rtype: numpy array
        """
        return cv.morphologyEx(im, cv.MORPH_OPEN, kernel)

    @staticmethod
    def morf_closing(im, kernel=cv.getStructuringElement(cv.MORPH_ELLIPSE, (30,30))):
        """Performs morphological closing

        :param im: Image to perform morphological closing
        :type im: numpy array
        :param kernel: Structuring element , defaults to cv.getStructuringElement(cv.MORPH_ELLIPSE, (30,30))
        :type kernel: numpy array, optional
        :return: Processed image
        :rtype: numpy array
        """
        return cv.morphologyEx(im, cv.MORPH_CLOSE, kernel)

    @staticmethod
    def gradient(im):
        """Performs image gradient calculation

        :param im: Image to calculate gradient
        :type im: numpy array
        :return: (gradient magnitude, gradient angle in degrees)
        :rtype: tuple
        """
        dx, dy = cv.spatialGradient(im, ksize=3)  # uses sobel
        mag = np.sqrt(dx**2.0 + dy**2.0)
        dir_deg = np.arctan2(dy, dx)  * (180 / np.pi)  # in degrees

        return mag, dir_deg

    @staticmethod
    def thresholds(data, offset_low=2, const_low=1.5, const_high=0.07):
        """Calculates dynamically thresholds in the image for Canny detector

        :param data: gradient of the image
        :type data: numpy array
        :param offset_low: Offset to exclude 0, defaults to 2
        :type offset_low: int, optional
        :param const_low: Factor of low threshold, defaults to 1.5
        :type const_low: float, optional
        :param const_high: Factor of high threshold, defaults to 0.07
        :type const_high: float, optional
        :return: (low threshold, high threshold)
        :rtype: tuple
        """
        return int((np.min(data)+offset_low)*const_low), int(np.max(data)*const_high)


class DetectionAlgorithms:
    """Classes holding the detection algorithms.
    
    New algorithms should be implemented as another abstract method
    """
    def __init__(self) -> None:
        pass
    
    @staticmethod
    def detect_hough(im_orig, im, th):
        """Node detection algorithm based on Circle Hough Transform

        :param im_orig: Original image to visualize the detection
        :type im_orig: numpy array
        :param im: Preprocessed image
        :type im: numpy array
        :param th: High threshold
        :type th: int
        :return: (circle instances, original image with detections)
        :rtype: tuple
        """
        circles = cv.HoughCircles(im, cv.HOUGH_GRADIENT, 1, np.shape(im_orig)[0] / 4,
                               param1=th, param2=9,
                               minRadius=20, maxRadius=50)
        if circles is not None:
            # convert the (x, y) coordinates and radius of the circles to integers
            circlesRound = np.round(circles[0, :]).astype("int")
            # loop over the (x, y) coordinates and radius of the circles
            for (x, y, r) in circlesRound:
                cv.circle(im_orig, (x, y), r, (242, 104, 201), thickness=3)
        
        return circles, im_orig
