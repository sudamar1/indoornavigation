"""
This script implemenents ploting and handler functions to analyze detection techniques.
Furthermore, allows tuning detection parameters and employ different methods to an image. 

Author: Martin Suda
Date: 22/05/2022
"""


from typing import Tuple
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from time import time
from detection import DetectionProcessing as dtpro
from detection import DetectionAlgorithms as dtalg
from skimage.morphology import disk
import os


def surf(data, shp:Tuple, cmap=cm.coolwarm):
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    x = np.arange(0, shp[0], 1)
    y = np.arange(0, shp[1], 1)
    x, y = np.meshgrid(y, x)

    ax.plot_surface(x, y, data, cmap=cmap,
                       linewidth=0, antialiased=False)
    ax.set_title('Gradient Magnitude')
    return fig, ax


def img_gradient(im):
    # dx = cv.Sobel(im, cv.CV_64F, 1, 0, ksize=3)
    # dy = cv.Sobel(im, cv.CV_64F, 0, 1, ksize=3)
    dx, dy = cv.spatialGradient(img, ksize=3) # uses sobel

    mag = np.sqrt(dx**2.0 + dy**2.0)
    dir = np.arctan2(dy, dx) 
    dir_deg = dir * (180 / np.pi) # in degrees

    return mag, dir_deg


def plot_orig_and_blurred_img(im, blurred):
    plt.figure()
    plt.subplot(121)
    plt.imshow(im, cmap='gray', vmin=0 ,vmax=255)
    plt.title("Original image")
    plt.axis("off")
    plt.subplot(122)
    plt.imshow(blurred, cmap='gray', vmin=0 ,vmax=255)
    # plt.imshow(cv.cvtColor(img_gauss, cv.COLOR_BGR2RGB)) # reorder colors BGR->RGB
    plt.title("Blurred image")
    plt.axis("off")


def degrade_background(orig_im, th, const):
    im = orig_im.copy()
    im[im<th] = im[im<th]*const
    
    return im


def detect_nodes(im, orig_im, th):
    im_det = orig_im.copy()
    circles = cv.HoughCircles(im, cv.HOUGH_GRADIENT, 1, ishape[0] / 8,
                               param1=th, param2=9,
                               minRadius=15, maxRadius=70)
    if circles is not None:
        # convert the (x, y) coordinates and radius of the circles to integers
        circlesRound = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circlesRound:
            cv.circle(im_det, (x, y), r, (0, 255, 0), 4)
    
    return circles, im_det


def read_image(path):
    img = cv.imread(path, cv.IMREAD_GRAYSCALE) # reads pic in BGR!!
    ishape = np.shape(img)  # rows, collumns

    return img, ishape


def get_thresholds(data):
    th_l = int((np.min(data)+2)*1.5)
    th_h = int(np.max(data)*0.07)
    
    return th_l, th_h

def print_results(ishape, ths, circles):
    print(f'img{n}.jpg{"":<25}|')
    print(35*'-')
    print(f"{'Image:':<24}{ishape}")
    print(f"{'Threshold:':<29}({ths[0]}, {ths[1]})")
    if circles is None:
        num_circ = 0
        print(f"{'Detected obj:':<30}{num_circ}")
    else:
        num_circ = np.shape(circles)[1]
        print(f"{'Detected obj:':<30}{num_circ}")
        for i, r in enumerate(circles[0]):
            tmp = f"Radius {i+1}:"
            print(f"{'':>5}{tmp:<25}{r[2]:.2f}")
    print(35*'-')
    print(f'{"Image load exec time ->":<30}{t1-t0:.6f}s')
    print(f'{"Blur exec time ->":<30}{t2-t1:.6f}s')
    print(f'{"Degradation exec time ->":<30}{t3-t2:.6f}s')
    print(f'{"Morf. Opening exec time ->":<30}{t4-t3:.6f}s')
    print(f'{"Grad exec time ->":<30}{t5-t4:.6f}s')
    print(f'{"Ths exec time ->":<30}{t6-t5:.6f}s')
    print(f'{"Canny exec time ->":<30}{t7-t6:.6f}s')
    print(f'{"Detection exec time ->":<30}{t8-t7:.6f}s')
    print(35*'-')
    print(f'{"Total ->":<30}{t7-t0:.6f}s')
    print(35*'=')

def plot_details(img=None, img_b=None, img_deg=None, g_mag=None, 
                ishape=None, edges=None, im_detected=None, img_open=None):
    if not img is None and not img_deg is None: 
        plt.figure()
        plt.subplot(121)
        plt.imshow(img, cmap='gray', vmin=0, vmax=255), plt.axis('off')  
        plt.title('Original image')
        plt.subplot(122)
        plt.imshow(img_deg, cmap='gray', vmin=0, vmax=255), plt.axis('off')
        plt.title('Background degradation or Binarization')

    if not img_b is None:
        plt.figure()
        plt.imshow(img_b, cmap='gray', vmin=0, vmax=255), plt.axis('off')
        plt.title('Blurred image')

    if not g_mag is None: 
        surf(g_mag, ishape)
    
    if not img_open is None: 
        plt.figure()
        plt.imshow(img_open), plt.axis('off')
        plt.title('Morphological opening')
    
    if not edges is None:
        plt.figure()
        plt.imshow(edges), plt.axis('off')
        plt.title('Canny edge detection')

    if not img is None and not im_detected is None: 
        plt.figure()
        plt.subplot(121)
        plt.imshow(img, cmap='gray', vmin=0, vmax=255), plt.axis('off')
        plt.title('Original image')
        plt.subplot(122)
        plt.imshow(im_detected, cmap='gray', vmin=0 ,vmax=255), plt.axis('off')
        plt.title('Detected nodes')


if __name__ == "__main__":
    p = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data", "orig")

    # im_range = range(111,120)  # up to 9 images range
    im_range = range(135,136)    # only one image
    cycles = len(im_range)
    total = 0

    for sub, n in enumerate(im_range,1):
        ipath = os.path.join(p,f'img{n}.jpg')
        
        t0 = time()
        img, ishape, imcopy = dtpro.read(ipath)
        t1 = time()
        
        img_b = dtpro.blur(imcopy)
        # img_b = dtpro.gauss_blur(imcopy)
        t2 = time()
    
        # img_deg = dtpro.degrade_background(img_b)
        img_deg = dtpro.binarization(img_b.copy(), 245)
        t3 = time()

        # kernel = cv.getStructuringElement(cv.MORPH_CROSS, (50,50))
        # kernel = cv.getStructuringElement(cv.MORPH_ELLIPSE, (38,38))
        # kernel = disk(22)
        # img_open = dtpro.morf_opening(img_deg.copy(), kernel)
        t4 = time()
        
        g_mag, g_dir = dtpro.gradient(img)
        t5 = time()

        ths = dtpro.thresholds(g_mag)
        t6 = time()
        
        # edges = cv.Canny(img_deg, ths[0], ths[1], True)
        t7 = time()

        circles, im_detected = dtalg.detect_hough(img.copy(), img_deg.copy(), ths[1])
        t8 = time()

        # Execution times
        print_results(ishape, ths, circles)
        # Plotting details
        plot_details(img, img_b, img_deg, g_mag, ishape, None, im_detected)

        # Plot multiple images
        # plt.subplot(3,3, sub)
        # plt.imshow(im_detected, cmap='gray', vmin=0 ,vmax=255), plt.axis('off')
        # plt.title(f'img{n}.jpg')

        total += (t8-t0)

    print(f'{"Averadge exec time ->":<30}{total/cycles:.6f}s')
    plt.show()
