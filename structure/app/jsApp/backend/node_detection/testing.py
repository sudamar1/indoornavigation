"""This file implemenents testing helper functions used while model evaluation.

Functions:
    * save_image(im, cnt, path)
    * get_counter(path)
    * save_test_data(img, path)
    * save_exec_time(t, img_name, path)

Author: Martin Suda
Date: 22/05/2022
"""

import os
import cv2 as cv


def save_image(im, cnt, path):
    """Saves image to the path

    :param im: Image to save
    :type im: numpy array
    :param cnt: Number of the image
    :type cnt: int
    :param path: Path to save the image
    :type path: str
    :return: counter
    :rtype: int
    """
    cnt += 1
    im.save(os.path.join(path, f"img{cnt}.jpg"))
    print(f"Image saved in ./pics/img{cnt}.jpg")
    return cnt


def get_counter(path):
    """Get the image count in the path

    :param path: Path to evaluate counter
    :type path: str
    :return: counter
    :rtype: int
    """
    return len(os.listdir(path))


def save_test_data(img, path):
    """Saves the image using OpenCV API.

    :param img: Image to save
    :type img: numpy array
    :param path: Path to save the image
    :type path: str
    :return: (counter, image name)
    :rtype: tuple
    """
    cnt = get_counter(path) + 1
    img_name = f"img{cnt}.jpg"
    cv.imwrite(os.path.join(path, img_name), img)  # testing purposes!

    return cnt, img_name


def save_exec_time(t, img_name, path):
    """Save the execution time to a prepared csv file with following format

    e.g.,
        name;exec;
        {img_name};{t};
        ...

    :param t: Execution time to save
    :type t: float
    :param img_name: Name to save the execution time under
    :type img_name: str
    :param path: Path to the .csv file
    :type path: str
    """
    with open(path, 'a+') as f:
        f.seek(0)
        data = f.read(100)
        if len(data) > 0:
            f.write('\n')
        f.write(f'{img_name};{t};')