"""This module implemenents node detection fuctionality. 
    
See detection.py for detection processing and algorithms.
See testing.py for testing functions.
See analysis.py for detection analysis.
See test directory to find latest test model results and data.

Classes:
    * DetectionProcessing - static class comprises processing methods
        * read
        * read_bytes
        * blur
        * gauss_blur
        * binarization
        * morf_opening
        * morf_closing
        * gradient
        * thresholds
    * DetectionAlgorithms - static class coprises detection methods
        * detect_hough

Functions:
    * save_image(im, cnt, path)
    * get_counter(path)
    * save_test_data(img, path)
    * save_exec_time(t, img_name, path)

Author: Martin Suda
Date: 22/05/2022
"""