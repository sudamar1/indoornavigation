"""This module implemenents the system application's Back End. 
    
See database for the MongoDB API.
See gateway_api for  the system gateway's API.
See node_detection for node detection.
See utils for helper functionality, such as Nodes class or flask directories management.

Author: Martin Suda
Date: 22/05/2022
"""