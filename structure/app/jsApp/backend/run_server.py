"""This file implemenents the core application's API. 

flask, cv2 libraries required.

Functions:
    * home_page()
    * get_nodes()
    * create_node()
    * add_node()
    * delete_node()
    * save_node_configuration()
    * robot_connection()
    * fetch_image()

Author: Martin Suda
Date: 22/05/2022
"""

import sys
from os.path import dirname

sys.path.append(dirname(__file__))

from flask import Flask, render_template, request
from flask.json import jsonify
from utils.node import Nodes
from utils.helper import get_directories
from gateway_api.g_api import GatewayAPI
from database.db import Database
from urllib import request as req
from urllib.error import URLError
from database.const import MONGO_COLL_ROBOT, MONGO_DB_NAME, MONGO_COLL_NODES
from node_detection.detection import DetectionProcessing as dtproc
from node_detection.detection import DetectionAlgorithms as dtalg
from json import loads
import cv2 as cv
import base64


# Global variables 
STATIC_FOLDER, TEMPLATE_FOLDER = get_directories()
mongo = Database()
client = mongo.client
app = Flask(__name__, static_folder=STATIC_FOLDER, template_folder=TEMPLATE_FOLDER)


@app.route('/')
def home_page():
    """Renders the application on the initial call.

    :return: Rendered template of "index.html"
    :rtype: str
    """
    return render_template('index.html')
    
    
@app.route('/nodes')
def get_nodes():
    """Getter of registered nodes. It takes nodes from Back End's database.

    :return: Distionary of all system nodes
    :rtype: dict
    """
    return Nodes.get_all_from_db(client)


@app.route('/nodes/create', methods=['POST'])
def create_node():
    """Create node request handler.

    :return: Request response
    :rtype: flask.Response
    """
    data = request.json
    app.logger.info(f'POST Data recieved:\n{data}')
    g_api = GatewayAPI(mongo.client)
    g_api.connect_api()
    c_code, resps = g_api.create_node(data)
    d_code = mongo.add_node_to_database(data)
    app.logger.info(f'Status of node creation: {c_code}')
    app.logger.info(f'Add node to database: {d_code}')

    # Creating response object
    if d_code == 11000:
        status_code = 400
        tmp = 'Node already exists!'
    else:
        status_code = d_code
        tmp = 'success' if d_code == 200 else 'fail'

    resp = jsonify({
        "d_save_status": d_code,
        "g_save_status": c_code,
        "d_content": tmp,
        "g_content": loads(resps[0].content),
        "g_content_keys": loads(resps[1].content)
    })
    resp.status_code = status_code
    resp.mimetype = "json"

    return resp


@app.route('/nodes/add', methods=['POST'])
def add_node():
    """Add node request handler.

    :return: Request response
    :rtype: flask.Response
    """
    data = request.json
    app.logger.info(f'POST Data recieved:\n{data}')
    g_api = GatewayAPI(mongo.client)
    # Add node check if it really exist in gateway based on eui
    g_api.connect_api()
    flag, node = g_api.get_node(data) # {status: 200, node_obj: {}}
    if flag:
        tmp = loads(node.content)
        tmp['error'] += ' in the GW. Use create node instead!'
        return jsonify({
            "d_save_status": node.status_code,
            "d_content": tmp
        })
    data['app_id'] = node['device']['applicationID']
    d_code = mongo.add_node_to_database(data)
    # app.logger.info(f'Status of node creation: {c_code}')
    app.logger.info(f'Add node to database: {d_code}')

    if d_code == 11000:
        status_code = 400
        tmp = 'Node already exists!'
    else:
        status_code = d_code
        tmp = 'success' if d_code == 200 else 'fail'
    # Creating response object
    resp = jsonify({
        "d_save_status": d_code,
        "d_content": {'error': tmp},
    })

    resp.status_code = status_code
    
    resp.mimetype = "json"

    return resp


@app.route('/nodes/delete', methods=['POST'])
def delete_node():
    """Delete node request handler.

    :return: Request response
    :rtype: flask.Response
    """
    data = request.json
    app.logger.info(f'POST Data recieved:\n{data}')
    g_api = GatewayAPI(mongo.client)
    g_api.connect_api()
    c_resp = g_api.delete_node(data)
    d_code = mongo.delete_node_from_database(data)
    c_code = c_resp.status_code
    c_cont = loads(c_resp.content)
    d_content = 'document does not exist' if d_code == 404 else 'document deleted'
    app.logger.info(f'Delete node from gateway: {c_code}: {c_cont}')
    app.logger.info(f'Delete node from database: {d_code}')

    # Creating response object
    resp = jsonify({
        "d_del_status": d_code,
        "g_del_status": c_code,
        "g_content": loads(c_resp.content),
        "d_content": d_content
    })
    resp.status_code = 200 if c_code == 200 and d_code == 200 else 404
    resp.mimetype = "json"

    return resp


@app.route('/nodes/config/save', methods=['POST'])
def save_node_configuration():
    """Save node configuration request handler. It saves last successful configuration to db.

    :return: Request response
    :rtype: flask.Response
    """
    app.logger.info('here')
    data = request.json
    app.logger.info(data)
    result = mongo.add_element(
        {"_id": data["devEUI"], "lastConfig": data["config"]},
        MONGO_COLL_NODES)

    if result.acknowledged:
        return "OK", 200

    return "FAIL", 404


@app.route('/robot/connection', methods=['POST'])
def robot_connection():
    """Robot connection request handler. Initiates and controls the connection. 

    :return: Request response
    :rtype: flask.Response
    """
    ip = request.data.decode('utf8')
    app.logger.info(ip)
    try:
        print(f'http://{ip}')
        resp = req.urlopen(f'http://{ip}', timeout=3)
        if resp.status == 200:
            mongo.add_element({"_id": 1, "ip": ip}, MONGO_COLL_ROBOT)
        
        return resp.read().decode('utf-8'), 200

    except URLError as exp:
        app.logger.info('Robot not reached! Check if properly connected in the network')
        app.logger.info(f'Reason: {exp.reason}')

    return 'Robot not reached! Check if properly connected in the network', 404


@app.route('/robot/fetch', methods=['GET'])
def fetch_image():
    """Fetch image request handler. Fetches image from robot and delegates it to Front End.

    :return: Request response with image content
    :rtype: flask.Response
    """
    coll = client[MONGO_DB_NAME][MONGO_COLL_ROBOT]
    ip_doc = coll.find_one({"_id": 1})
    
    if ip_doc:
        try:
            url = f'http://{ip_doc["ip"]}/photo.jpg'
            with req.urlopen(url, timeout=3) as img_resp:
                img_resp = req.urlopen(url, timeout=3)
        except:
            print('Robot connection failed. Image not received.')
            return 'Robot connection failed. Image not received.', 404
        
        # img, _, imcopy = dtproc.read_bytes(img_resp.read())
        img, _, imcopy = dtproc.read_bytes_color(img_resp.read())
        binar = dtproc.binarization(dtproc.blur(imcopy))
        th = dtproc.thresholds(dtproc.gradient(imcopy)[0])[1]
        _, im_detected = dtalg.detect_hough(img, binar, th)

        # TODO: transfer to dtproc library
        _, buff = cv.imencode('.jpg', im_detected)
        im = base64.b64encode(buff).decode('utf-8')
        
        return im, 200
    else:
        print('no ip provided! First check connection')
        return('No robot ip provided! Check the connection first!'), 404

# if __name__ == "__main__":
    # app.run(host='0.0.0.0', port=8000)

