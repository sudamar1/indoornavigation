"""This file implemenents Nodes and Node class. It suppose to improve node handling within the structure.

Classes:
    * Nodes
    * Node

Author: Martin Suda
Date: 22/05/2022

Revision:
22/05/2022 - Classes hasn't been incorporated within the server structure at the moment.
"""


from database.const import MONGO_DB_NAME, MONGO_COLL_NODES


class Nodes:
    """This class creates container for system nodes and enables to easily handle multiple nodes.
    """
    def __init__(self, nodes=None):
        self.nodes = nodes
    
    def get_names(self):
        names = []
        for node in self.nodes:
            names.append(node["name"])
        return names
        
    def get_json(self):
        nodes_j = {}
        for node in self.nodes:
            nodes_j["name"] = node.get_json()
        return nodes_j
    
    @staticmethod
    def get_all_from_db(db_client, db=MONGO_DB_NAME, coll=MONGO_COLL_NODES):
        """Gets all existing nodes from Back End database.

        :param db_client: MongoDB client object
        :type db_client: pymongo.MongoClient
        :param db: MongoDB Database object to search nodes, defaults to MONGO_DB_NAME
        :type db: pymongo.Database, optional
        :param coll: MongoDB Collection object to search nodes, defaults to MONGO_COLL_NODES
        :type coll: pymongo.Collection, optional
        """
        def process_db_data(data):    
            tmp = {}
            for node in data:
                tmp[node['_id']] = {key: val for key, val in node.items() if key != '_id'}
            print(f"Collected data from DB:\n{tmp}")
            return tmp

        return process_db_data(db_client[db][coll].find({}))

    
class Node:
    """This class a Node object to support easier node handling on the Back End.
    """
    def __init__(self, data):
        self.j = data
        self.name = data['name']
        self.tx_topic = data['tx']
        self.rx_topic = data['rx']

    def get_name(self):
        return self.name

    def get_json(self):
        return self.j
