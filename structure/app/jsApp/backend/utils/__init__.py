"""This module implemenents helper functions and classes neccessiated by the structure. 
    
See helper.py for flask directory management, mac and ip handling.
See node.py for Nodes and Node classes. -- NOT USED IN THE STRUCTURE YET

Author: Martin Suda
Date: 22/05/2022
"""

import os, sys;
sys.path.append(os.path.dirname(os.path.realpath(__file__)))