"""This file implemenents the core application's API. 

subprocess, re, socket, netfaces libraries required.

Functions:
    * get_directories()
    * get_macs_ip(rng=None, port=22)
    * ping_ip(ip, port, timeout=2)
    * ping_range_ips(max, port)

Author: Martin Suda
Date: 22/05/2022
"""


import os
import subprocess
import re
import socket
import netifaces

def get_directories():
    """Collects the neccessary paths to run Back End server

    :return: (path to the statics, path to the templates)
    :rtype: tuple
    """
    root = os.getcwd()
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    print(f"{'Temporary CWD:':>22} {os.getcwd()}")

    statics = os.path.abspath('../../frontend/statics/')
    templates = os.path.abspath('../../frontend/templates/')
    
    print(f"{'Templates directory:':>22} {statics}")
    print(f"{'Statics directory:':>22} {templates}")
    os.chdir(root)
    print(f"{'CWD:':>22} {os.getcwd()}")

    return statics, templates

def get_macs_ip(rng=None, port=22):
    """Gets all available macs and thier ips from the network.

    :param rng: Range of ips to look for, defaults to None
    :type rng: int, optional
    :param port: Port to ping the ips (wanted device listens on), defaults to 22
    :type port: int, optional
    :raises exp: Unwanted behaviour exception
    :return: Dictionary of macs and their ips
    :rtype: dict
    """
    if rng:
        ping_range_ips(rng, port)

    mac_ip = {}
    pat = re.compile('.+\((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\).at.((?:[0-9A-Fa-f]{1,2}:){5}(?:[0-9A-Fa-f]){1,2})')
    arp_out = subprocess.check_output('arp -a', stderr=subprocess.STDOUT, shell=True).decode('utf-8')
    lines = arp_out.split('\n')
    lines.pop()  # get rid off last element (blank)

    for line in lines:
        mtch = pat.match(line)
        try:
            mac_ip[mtch.group(2)] = mtch.group(1)
        except Exception as exp:
            if 'incomplete' in line:
                continue
            raise exp 

    return mac_ip

def ping_ip(ip, port, timeout=2):
    """Pings a IP address.

    :param ip: IP address
    :type ip: str
    :param port: Port to ping the IP address
    :type port: int
    :param timeout: Duration of a ping, defaults to 2
    :type timeout: int, optional
    :return: Status
    :rtype: Boolean
    """
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket.setdefaulttimeout(timeout)
    result = soc.connect_ex((ip, port))
    soc.close()
    print('done')
    return True if not result else False

def ping_range_ips(max, port):
    """Pings a range of IPs in the network

    :param max: Max ip address to ping (ONLY 255.255.255.0 mask supported)
    :type max: int
    :param port: Port to ping
    :type port: int
    :raises Exception: Internal error of netifaces module
    :raises Exception: Unsupported mask entered
    """
    en0 = netifaces.ifaddresses('en0')
    ip_loc = socket.gethostbyname(socket.gethostname())
    try: 
        tmp = en0[2]
        mask = None
        for addr in tmp:
            ip = addr['addr']
            if ip_loc == ip:
                mask = en0[2][0]['netmask']
                break
    except:
        raise Exception('Netifaces failed')

    if mask == '255.255.255.0':
        ip_loc = ip_loc.split('.')
        ip_net = f'{ip_loc[0]}.{ip_loc[1]}.{ip_loc[2]}.'
        for num in range(1,max):
            ip = f'{ip_net}{num}'
            ping_ip(ip, port)
    else:
        raise Exception('Unsupported mask')

