"""This file consists of database structure constants utilized in the module.

"""

MONGO_DB_NAME = "inavi"
MONGO_COLL_NODES = "nodes"
MONGO_COLL_AUTH = "auth"
MONGO_COLL_ROBOT = "robot"
MONGO_COLL_GW = "gw"