"""This script implemenents database initialization. It creates project structure on the new device. 


pymongo library required.
MongoDB server required
    NOTE: MongoDB server MUST be running while executing this script.
    See: https://www.mongodb.com/docs/guides/server/install/ for further installation intructions.

Functions:
    * init_client(adr=None)
    * create_inavi_database(client)
    * create_collections(db)
    * load_nodes_to_db(db, nodes_eui=["303636326839780a","303636327939760b","303636325639730a"])
    * check_init(client, db, colls, num_nodes)

Author: Martin Suda
Date: 22/05/2022
"""

# import sys
# from os.path import dirname

# sys.path.append(dirname(__file__))

import pymongo
from pymongo.errors import DuplicateKeyError 
import const
# from database.db import Database
from db import Database

def init_client(adr=None):
    """Connects the MongoDB server and creates a client.

    :param adr: MongoDB server address ("ip:port"); if None, 'localhost:27017' used, defaults to None
    :type adr: str, optional
    :return: MongoDB client
    :rtype: pymongo.MongoClient
    """
    if adr is None:
        adr = 'localhost:27017'
    return pymongo.MongoClient(f"mongodb://{adr}/")


def create_inavi_database(client):
    """Creates the project database.

    :param client: MongoDB client object
    :type client: pymongo.MongoClient
    :return: New project database
    :rtype: mongo.Database
    """
    return client[const.MONGO_DB_NAME]

def create_collections(db):
    """Creates all neccessary collections.

    :param db: MongoDB database object
    :type db: mongo.Database
    :return: List of collection names
    :rtype: list
    """
    colls = {item:getattr(const, item) for item in dir(const) if not item.startswith("__") and not item.endswith("__") and "_COLL_" in item}
    if colls is None:
        print("No collection to create")
        return False

    for coll in colls.values():
        tmp = db[coll]
        tmp.insert_one({"_id": 1})
        tmp.delete_many({})

    return colls


def load_nodes_to_db(db, nodes_eui=["303636326839780a","303636327939760b","303636325639730a"]):
    """Loads Nodes object to the MONGO_COLL_NODES collection

    :param db: MongoDB database object
    :type db: mongo.Database
    :param nodes_eui: dev_euis of nodes, defaults to ["303636326839780a","303636327939760b","303636325639730a"]
    :type nodes_eui: list, optional
    :return: Number of created nodes
    :rtype: int
    """
    for node in nodes_eui:
        n = Database.create_node_object({"deveui": node, "app_id": 1})
        db[const.MONGO_COLL_NODES].insert_one(n)

    return len(nodes_eui)

def load_macs(db, gw_mac="b8:27:eb:53:4a:0", rob_mac="a4:e5:7c:68:ea:80"):
    """Add mac addresses to the database

    :param db: Database object
    :type db: mongo.Database
    :param gw_mac: MAC of the Gateway, defaults to "b8:27:eb:53:4a:0"
    :type gw_mac: str, optional
    :param rob_mac: MAC of the Robot, defaults to "a4:e5:7c:68:ea:80"
    :type rob_mac: str, optional
    """

    cnt = db[const.MONGO_COLL_GW].update_one({"_id": 1}, {"$set" : {"mac": gw_mac}}, upsert=True).matched_count
    cnt += db[const.MONGO_COLL_ROBOT].update_one({"_id": 1}, {"$set" : {"mac": rob_mac}}, upsert=True).matched_count

    return cnt

def check_init(client, db, colls, num_nodes, mac_status):
    """Final database check if everything initialized properly.

    :param client: MongoDB client object
    :type client: pymongo.MongoClient
    :param db: MongoDB database object
    :type db: pymongo.Database
    :param colls: List of collection names obtained from create_collections()
    :type colls: list
    :param num_nodes: Number of created nodes obtained from load_nodes_to_db()
    :type num_nodes: int
    """
    if const.MONGO_DB_NAME in client.list_database_names():
        print(f"{'Database':<11}'{const.MONGO_DB_NAME}'{'':<3}created")
        
        coll_list = db.list_collection_names()
    for coll in colls.values():
        if coll not in coll_list:
            print(f"FAILED to create the collection '{coll}'")
        else:
            print(f"{'Collection':<11}'{coll:<5}'{'':<3}created")

    cnt = db[const.MONGO_COLL_NODES].count_documents({})
    print(f"{cnt}/{num_nodes} Nodes created")

    print(f"MAC addresses addeded: {mac_status==2 or mac_status==0}")


if __name__ == "__main__":
    # Run this script to initialize the projects MongoDB database
    # MongoDB server MUST be runninng on the device running this script!
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--address", 
                        help="Mongodb address (mongodb for docker, localhost for local machine)",
                        nargs='?',
                        const=1,
                        type=str)
    args = parser.parse_args()
    
    c = init_client(adr=args.address)
    d = create_inavi_database(c)
    cl = create_collections(d)
    n_nodes = load_nodes_to_db(d)
    macs = load_macs(d)
    check_init(c,d,cl,n_nodes,macs)