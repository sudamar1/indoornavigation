"""This module implemenents the MongoDB database API. 

See db.py for the core MongoDB API.
See const.py for database structure constants (database name, collection names)
See initializer.py to initialize database structure on new device

Author: Martin Suda
Date: 22/05/2022
"""

import sys
from os.path import dirname

sys.path.append(dirname(__file__))