"""This file implemenents the Database API. 

pymongo library required.

Classes:
    * Database
    Methods:
        * __init__(self, address=None)
        * connect(self, address=f"{DEFAULT_HOST}:{DEFAULT_PORT}")
        * add_element(self, data, coll)
        * delete_element(self, el_id, coll)
        * delete_node_from_database(self, data)
        * delete_node_from_database(self, data)
    Static Methods:
        * create_node_object(data)


Author: Martin Suda
Date: 22/05/2022
"""


from pymongo.errors import DuplicateKeyError
from const import MONGO_DB_NAME, MONGO_COLL_NODES
import pymongo


class Database:
    """This class implements the core MongoDB database interactions.

    It connects the initialized database and creates a database client.
    It handles querying of project related data.
    """
    DEFAULT_HOST = "mongodb"
    DEFAULT_PORT = 27017

    def __init__(self, address=None):
        self.client = None
        if not address:
            self.connect()
        
    def connect(self, address=f"mongodb://{DEFAULT_HOST}:{DEFAULT_PORT}/"):
        """Establishes the database connection.

        :param address: Database address in form IP:PORT, defaults to f"{DEFAULT_HOST}:{DEFAULT_PORT}"
        :type address: str, optional
        :return: A database client object
        :rtype: pymongo.MongoClient
        """
        self.client = pymongo.MongoClient(address)
        return self.client

    def add_element(self, data, coll):
        """Adds an document with "data" content to the collection "coll".

        :param data: Data to be in document
        :type data: dict
        :param coll: Collection to enter the new document
        :type coll: pymongo.Collection
        :return: Result 
        :rtype: pymongo.UpdateResult
        """
        c = self.client[MONGO_DB_NAME][coll]
        # {"ID" : "12345"},  {"ID" : "67891", "Account_Number" : "1234 5678 9101"}}
        return c.update_one({"_id": data["_id"]}, {"$set" : data}, upsert=True)

    def delete_element(self, el_id, coll):
        """Deletes an document with id "el_id" from the collection "coll".

        :param el_id: Document ID
        :type el_id: int, str
        :param coll: Collection from which to delete the document
        :type coll: pymongo.Collection
        :return: Result
        :rtype: pymongo.DeleteResult
        """
        c = self.client[MONGO_DB_NAME][coll]
        return c.delete_one({'_id': el_id})

    def add_node_to_database(self, data):
        """Adds the Nodes object to MONGO_COLL_NODES collection.

        :param data: Nodes object
        :type data: Nodes (dict)
        :return: Status code
        :rtype: int
        """
        db = self.client[MONGO_DB_NAME]
        coll = db[MONGO_COLL_NODES]
        try:
            resp = coll.insert_one(self.create_node_object(data))
        except DuplicateKeyError as exp:  # err code 11000 
            # print(exp)
            return exp.code
        except Exception as exp:
            print(exp)
            return 500

        return 200
    
    def delete_node_from_database(self, data):
        """Deletes Nodes object from database.

        :param data: Nodes object
        :type data: Nodes (dict)
        :return: Status code
        :rtype: int
        """
        db = self.client[MONGO_DB_NAME]
        coll = db[MONGO_COLL_NODES]
        try:
            resp = coll.delete_one({"_id": data['deveui']})
            if resp.deleted_count == 0:
                return 404

        except Exception as exp:
            print(exp)
            return 500
        return 200
        
    @staticmethod
    def create_node_object(data):
        """Creates Nodes object from dictionary.

        :param data: Dictionary containing node data
                e.g., 
                    {
                    "name":"dummy",
                    "app_id":"1",
                    "desc":"dummy",
                    "deveui":"0000000000000000",
                    "profid":"bdb952bb-0424-4fd2-90df-63da9c30fc99",
                    "alt":"0",
                    "fcnt":True,
                    "tag":"",
                    "var":"",
                    "appKey":"2b7e151628aed2a6abf7158809cf4f11",
                    "nwkKey":"2b7e151628aed2a6abf7158809cf4f11"}
        :type data: dict
        :return: Nodes object
        :rtype: dict
        """
        name = data["deveui"]
        id = data["deveui"]
        tx = f'application/{data["app_id"]}/device/{id}/tx'
        rx = f'application/{data["app_id"]}/device/{id}/rx'
        txack = f'application/{data["app_id"]}/device/{id}/txack'
        
        return {"_id": id, "name": name, "tx": tx, "rx": rx, "txack": txack}
        
