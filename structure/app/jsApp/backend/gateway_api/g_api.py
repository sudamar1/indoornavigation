"""This file implemenents the core API with gateway. 

json library required.

Classes:
    * GatewayAPI
    Methods:
        * __init__(self, db_client)
        * connect_api(self)
        * create_node(self, data)
        * get_node(self, data)
        * delete_node(self, data)
        * check_db_for_jwt(self)
        * check_validity_of_jwt(self)

Author: Martin Suda
Date: 22/05/2022
"""


from gateway_api.api_requests import get_request, post_request, delete_request ,eval_response_codes
from gateway_api import urls as urls
import gateway_api.payloads as payloads
import gateway_api.headers as h
import gateway_api.cred as cred
import gateway_api.validator as validator
from database.const import MONGO_DB_NAME, MONGO_COLL_AUTH
from json import loads, dumps


class GatewayAPI:
    def __init__(self, db_client) -> None:
        # self.address = urls.get_gw_api(db_client)
        self.db_client = db_client
        self.jwt_token = self.check_db_for_jwt()

    def connect_api(self):
        """Connects Back End to the gateway using jwt token validation.

        :return: None, if not connected jwt token otherwise
        :rtype: str, None
        """
        if self.jwt_token is None:
            headers = h.get_login_headers()
            data = dumps({"password": cred.pswd,"username": cred.username})
            resp = post_request(urls.get_login(self.db_client), data=data, headers=headers, reason='Login')
            if resp.status_code != 200:
                print('G-API: Login Failed!')
                print(resp.text)
                return None

            self.jwt_token = loads(resp.content)["jwt"]
            auth = self.db_client[MONGO_DB_NAME][MONGO_COLL_AUTH]
            auth.insert_one({"_id": 1, "jwt": self.jwt_token})
            print('G-API: Login Successful')
        else:
            if self.check_validity_of_jwt() != 200:
                print("G-API: JWT Expired.")
                auth = self.db_client[MONGO_DB_NAME][MONGO_COLL_AUTH]
                auth.delete_many({})
                self.jwt_token = self.check_db_for_jwt()
                print("G-API: JWT Deleted.")
                print("G-API: Fetching new JWT...")
                self.connect_api()
            else:
                print('G-API: Login Successful')            

        return self.jwt_token

    def create_node(self, data):
        """Creates node in the gateway.

        :param data: Data from frontend
                e.g.,
                {
                "name":"dummy",
                "app_id":"1",
                "desc":"dummy",
                "deveui":"0000000000000000",
                "profid":"bdb952bb-0424-4fd2-90df-63da9c30fc99",
                "alt":"0",
                "fcnt":True,
                "tag":"",
                "var":"",
                "appKey":"2b7e151628aed2a6abf7158809cf4f11",
                "nwkKey":"2b7e151628aed2a6abf7158809cf4f11"}
        :type data: dict
        :return: Status code
        :rtype: int
        """
        validator.create_node_data(data)
        data_in_order = (data["app_id"],data["desc"],data["deveui"],
                            data["profid"], data["name"],data["alt"],
                            data["fcnt"],data["tag"],data["var"])
        payload = payloads.get_create_node_payload(*data_in_order)
        headers = h.get_jwt_headers_post(self.jwt_token)
        resp_create = post_request(urls.get_devices(self.db_client), data=payload, headers=headers, reason="Create Node")
        
        payload = payloads.get_keys_payload(data["appKey"], data["deveui"], data["nwkKey"])
        resp_keys = post_request(urls.get_keys_adr(self.db_client,
                                                    data["deveui"]), data=payload, 
                                                    headers=headers, reason="Add Keys to the node")
        
        return eval_response_codes([resp_create, resp_keys]), (resp_create, resp_keys)

    def get_node(self, data):
        """Gets a node from gateway by DEV EUI.

        :param data: Dictionary containing DEV EUI
        :type data: dict
        :return: Flag, Response
        :rtype: Boolean, requests.Response
        """
        url = urls.get_node_by_deveui_adr(self.db_client, data['deveui'])
        headers = h.get_jwt_headers_get(self.jwt_token)
        resp = get_request(url, headers=headers, reason=f"Get Node with eui {data['deveui']}")
        if resp.status_code == 200:
            return False, loads(resp.content)
        print(f"G-API GET: Get node failed! => status {resp.status_code}: {loads(resp.content)['error']}")
        return True, resp

    def delete_node(self, data):
        """Deletes a node from gateway based on DEV EUI.

        :param data: Dictionary containing DEV EUI
        :type data: dict
        :return: Response
        :rtype: response.Response
        """
        url = urls.delete_node_by_deveui_adr(self.db_client, data['deveui'])
        headers = h.get_jwt_headers_get(self.jwt_token)
        resp = delete_request(url, headers=headers, reason=f"Delete Node with eui {data['deveui']}")
        if resp.status_code != 200:
            print(f"G-API GET: Get node failed! => status {resp.status_code}: {loads(resp.content)['error']}")
        return resp

    def check_db_for_jwt(self):
        """Checks the database for jwt token existence.

        :return: None, if no jwt token, jwt token otherwise
        :rtype: str, None
        """
        auth = self.db_client[MONGO_DB_NAME][MONGO_COLL_AUTH]
        coll_out = auth.find_one({})
        return coll_out if coll_out is None else coll_out["jwt"]

    def check_validity_of_jwt(self):
        """Checks the validity of jwt token. 

        :return: Status code, 200 if successful, invalid otherwise
        :rtype: int
        """
        return get_request(urls.get_applications_adr(self.db_client, 1),
                            headers=h.get_jwt_headers_get(self.jwt_token),
                            reason="JWT Check").status_code

