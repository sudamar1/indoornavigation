"""This file implemenents header getters unique for the gateway API. 

Functions:
    * get_jwt_headers_post(jwt)
    * get_jwt_headers_get(jwt)
    * get_login_headers()

Author: Martin Suda
Date: 22/05/2022
"""


def get_jwt_headers_post(jwt):
    """Gets headers needed for gateway POST requests.

    :param jwt: Valid jwt token
    :type jwt: str
    :return: Headers
    :rtype: dict
    """
    return {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Grpc-Metadata-Authorization': f'Bearer {jwt}',
            }

def get_jwt_headers_get(jwt):
    """Gets headers needed for gateway GET requests.

    :param jwt: Valid jwt token
    :type jwt: str
    :return: Headers
    :rtype: dict
    """
    return {
            'Accept': 'application/json',
            'Grpc-Metadata-Authorization': f'Bearer {jwt}',
            }

def get_login_headers():
    """Gets headers needed for successful log in.
    """
    return {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
            }