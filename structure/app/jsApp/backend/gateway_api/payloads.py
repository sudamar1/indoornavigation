"""This file implemenents payload creators unique to the gateway API communication. 

json library required.

Functions:
    * get_keys_payload(*args)
    * get_create_node_payload(*args)


Author: Martin Suda
Date: 22/05/2022
"""


from json import dumps


def get_keys_payload(*args):
    """Gets the application key and network key from the ordered data input.
        e.g., application key, device eui, network key

    :return: String dictionary contaning request payload
    :rtype: str
    """
    return dumps({"deviceKeys": {"appKey": args[0], "devEUI": args[1],"nwkKey": args[2]}})


def get_create_node_payload(*args):
    """Gets node payload from ordered data input in this order;
        e.g., applicationID, description, device eui, device profileID, name, reference alt., skipFcntCheck,
            tags, variables

    :return: String dictionary contaning request payload
    :rtype: str
    """
    return dumps(
        {"device": 
            {"applicationID": args[0], 
            "description": args[1], 
            "devEUI": args[2], 
            "deviceProfileID": args[3],
            "name": args[4],
            "referenceAltitude": args[5],
            "skipFCntCheck":args[6] , 
            "tags": args[7], 
            "variables": args[8]}
        })
