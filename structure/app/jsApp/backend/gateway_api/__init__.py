"""This module implemenents the system gateway's API.
    
See api_requests.py for HTTP requests.
See cred.py for the credentials. -- NEEDS TO BE MOVED TO DB
See g_api.py for the core gateway's API.
See headers.py for the HTTP headers.
See payloads.py for the request's payloads.
See urls.py for request's urls.
See validator.py for data validation.

Author: Martin Suda
Date: 22/05/2022

Revision:
22/05/2022 - Currently implements node management and API authentication.
"""