"""This file implemenents a custom Error handler and validator of node data received from Front End. 

json library required.

Functions:
    * get_keys_payload(*args)
    * get_create_node_payload(*args)


Author: Martin Suda
Date: 22/05/2022
"""


class DataValidationError(Exception):
    def __init__(self, message):
        super().__init__(message)

def create_node_data(data):
    """Validates the correctness of node data from Front End

    :param data: Data received from Front End
    :type data: dict
    :raises DataValidationError: If validation fails a custom validation error is raised
    """
    if data["var"]=="":
        data["var"] = {}
    if data["tag"]=="":
        data["tag"] = {}
    if len(data["deveui"]) != 16:
        raise DataValidationError("Dev EUI must be 8 bytes!")
    if len(data["appKey"]) != 32 or len(data["nwkKey"]) != 32:
        raise DataValidationError("Application and Network Key must be 16 bytes!")
    
    if data["fcnt"] == "true":
        data["fcnt"] = True
    elif data["fcnt"] == "false":
        data["fcnt"] = False
    else:
        raise DataValidationError(f'Fcnt must be true or false! value is {data["fcnt"]}')


