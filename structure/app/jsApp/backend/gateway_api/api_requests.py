"""This file implemenents HTTP requests and other HTTP communication helper functions. 

requests library required.

Classes:
    * WrongRequestResponse(Exception)

Functions:
    * post_request(url, data, headers, reason="")
    * get_request(url, headers, reason)
    * delete_request(url, headers, reason)
    * eval_response_codes(resps)

Author: Martin Suda
Date: 22/05/2022
"""


import requests


def post_request(url, data, headers, reason=""):
    """Sends a POST request.

    :param url: Request url
    :type url: str
    :param data: Request data
    :type data: any
    :param headers: Request headers
    :type headers: dict
    :param reason: Purpose of the request, defaults to ""
    :type reason: str, optional
    :return: Response
    :rtype: requests.Response
    """
    print(f'G-API POST: Sending request "{reason}"')
    return requests.post(url, data=data, headers=headers)


def get_request(url, headers, reason):
    """Sends a GET request.

    :param url: Request url
    :type url: str
    :param headers: Request headers
    :type headers: dict
    :param reason: Purpose of the request
    :type reason: str
    :return: Response
    :rtype: requests.Response
    """
    print(f'G-API GET: Sending request "{reason}"')
    return requests.get(url=url, headers=headers)


def delete_request(url, headers, reason):
    """Sends a DELETE request.

    :param url: Request url
    :type url: str
    :param headers: Request headers
    :type headers: dict
    :param reason: Purpose of the request
    :type reason: str
    :return: Response
    :rtype: requests.Response
    """
    print(f'G-API DELETE: Sending request "{reason}"')
    return requests.delete(url=url, headers=headers)


def eval_response_codes(resps):
    """Evaluates multiple request status codes

    :param resps: List of status codes
    :type resps: list
    :return: Status code
    :rtype: int
    """
    for resp in resps:
        print(f'G-API EVL: Response code: {resp.status_code} => {resp.text}')
        if resp.status_code != 200:    
            return resp.status_code
    return 200


class WrongRequestResponse(Exception):
    """This class implements a custom exception handler.
    
    """
    def __init__(self, message) -> None:
        super().__init__(message)