"""This file implemenents gateway API url getters. 

Functions:
    * get_gw_api(db_client)
    * get_login(db_client)
    * get_login(db_client)
    * get_devices(db_client)
    * get_keys_adr(db_client, deveui)
    * get_applications_adr(db_client, limit)
    * get_node_by_deveui_adr(db_client, dev_eui)
    * delete_node_by_deveui_adr(db_client, dev_eui)

Author: Martin Suda
Date: 22/05/2022
"""


from database.const import MONGO_DB_NAME, MONGO_COLL_GW
from utils.helper import get_macs_ip
from utils.helper import ping_ip


LOGIN_SFIX = '/internal/login'
DEVICES_SFIX = '/devices'


def get_gw_api(db_client):
    gw_coll = db_client[MONGO_DB_NAME][MONGO_COLL_GW]
    try: 
        gw = gw_coll.find_one({"_id": 1})
        if 'ip' in gw:
            gw_ip = gw['ip']
            if ping_ip(gw_ip, 22, 4) and gw_ip == get_macs_ip()[gw["mac"]]:
                return f'http://{gw_ip}:8080/api'

        gw_ip = get_macs_ip(rng=10, port=22)[gw["mac"]]
        gw_coll.update_one({"_id": 1}, {"$set" : {"ip": gw_ip}}, upsert=True)

    except Exception as exp:
        print(exp)
        raise Exception('GW ip not found!')
    
    return f'http://{gw_ip}:8080/api'
    

def get_login(db_client):
    return get_gw_api(db_client) + LOGIN_SFIX


def get_devices(db_client):
    return get_gw_api(db_client) + DEVICES_SFIX


def get_keys_adr(db_client, deveui):
    return get_gw_api(db_client) + f'/devices/{deveui}/keys'


def get_applications_adr(db_client, limit):
    return get_gw_api(db_client) + f'/applications?limit={limit}'


def get_node_by_deveui_adr(db_client, dev_eui):
    return get_gw_api(db_client) + f'/devices/{dev_eui}'


def delete_node_by_deveui_adr(db_client, dev_eui):
    return get_gw_api(db_client) + f'/devices/{dev_eui}'