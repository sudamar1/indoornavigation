/*
  LoraStar.ino

  Provides an example to use the shield as a simple LoRa radio.
  Data are exchange between multiple boards. User sets one master, the rest must be slaves and
  only recieve messages. User will then set a pwm out for each slave.

*/

#include "LoRaRadio.h"

#define SEND_PERIOD_MS 1000 //send period every second.

// Serial port use to communicate with the USI shield.
// By default, use D0 (Rx) and D1(Tx).
// For Nucleo64, see "Known limitations" chapter in the README.md
HardwareSerial SerialLora(PA12, PA11);

// Messages to exchange
char* Message;
int BufferSize = 0;
uint8_t PWM_Msg[4];
uint8_t OkMessage[] = "OKOKOKOK";

//Incomming string
String rx = ""; // Serial string
String RxLora = ""; // message from LoRa
//Device name
String DeviceName = "D1";

bool isMaster = true;
bool next = true;
int timer = 0;

//PWM variables
float DutyCycle = 0;
float Frequency = 0;
bool PWM_Ready = false;

//button detection
int USER_BUTTON = 2;
int buttonState = 0;         // current state of the button
int lastButtonState = 0;     // previous state of the button

void PWM_period(int pin,float dutycycle,float frequency){
  
  int period = (1/frequency)*1000;
  digitalWrite(pin, HIGH);
  delay(period*dutycycle/100);
  digitalWrite(pin, LOW);
  delay(period - period*dutycycle/100);
}

char* Set_Lights_PWM(){
  String temp = "";
  String connector = "#";
  String 
  rx = "";
  Serial.println("Enter device name\n");
  while(Serial.available() == 0){
    rx = Serial.readString();
    if(rx != ""){

      for(int i = 0;i < rx.length() - 1;i++){
        temp += rx[i];    
      }
      temp += connector;
      rx = "";
      break;
    }
  }

  //Serial.println("RX: " + rx + "\n");


  //DEBUG
  //Serial.println("TEMP: " + temp + "\n");
  
  
  Serial.println("Enter PWM dutycycle\n");
  while(Serial.available() == 0){
    rx = Serial.readString();
    if(rx != ""){

      for(int i = 0;i < rx.length() - 1;i++){
        temp += rx[i];   
      }
      temp += connector;
      rx = "";
      break;
    }
  }
  //Serial.println("RX: " + rx + "\n");
  //Serial.println("TEMP: " + temp + "\n");
  
  Serial.println("Enter PWM frequency\n");
  while(Serial.available() == 0){
    rx = Serial.readString();
    if(rx != ""){

      for(int i = 0;i < rx.length() - 1;i++){
        temp += rx[i];
      }
      temp += connector + "@";
      rx = "";
      break;
    }
  }
  Serial.println("TEMP: " + temp + "\n");
  Serial.println("TEMP length: " + String(temp.length()) + "\n");
  char* data = new char[temp.length()+1];
  temp.toCharArray(data, temp.length()+1);
  BufferSize = temp.length()+1;
  return data;
}

void RecieveLoraData(uint8_t* data){
  char* DataTransmitted = (char*)data;
  //Serial.println("GOT HERE!" + String(DataTransmitted));
  for(int i = 0; i < 4; i++){
    if(DataTransmitted[i] != '@'){
      RxLora += DataTransmitted[i];
    }
    else{
      SetPWMVariables(RxLora);
      RxLora = "";
    }
  }
  


}

void SetPWMVariables(String data){
  String temp[3]; //0 - device name, 1 - duty cycle, 2 - frequency
  int j = 0;
  char* NewData;
  data.toCharArray(NewData, data.length()+1);
  for(int i = 0; i < data.length()+1;i++){
    if(NewData[i] != '#'){
      temp[j] += NewData[i];
    }else{
      j++;
    }
  }
  if(temp[0] != DeviceName){
    Serial.println("Not the device name\n");
    return;
  }
  DutyCycle = temp[1].toFloat();
  Frequency = temp[2].toFloat();
}

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
  Serial.println("-- LoRa star sketch --");

  while(!loraRadio.begin(&SerialLora)) {
    Serial.println("LoRa module not ready");
    delay(1000);
  }

  Serial.println("LoRa module ready\n");
  delay(100);
  Serial.println("I am slave or a master?\n");
  Serial.println("Please enter master or slave.\n");

  while(Serial.available() == 0){

      rx = Serial.readString();
      if(rx == "master\n" || rx == "master" || rx == "master\r\n" || rx == "master\r"){
        Serial.println("This module will be: " + rx + "\n");
        isMaster = true;
        break;
      }

      if(rx == "slave\n" || rx == "slave" || rx == "slave\r\n" || rx == "slave\r"){
        Serial.println("This module will be: " + rx + "\n");
        isMaster = false;
        break;
      }
      
      
  }

  // hardcode pins for pwm
  pinMode(4, OUTPUT);
  // hardcode pin for user button
  pinMode(PC13, INPUT);
  //PWM DEBUG
  PWM_period(4,0.5,1);
}

void loop() {
  // put your main code here, to run repeatedly:
  uint8_t rcvData[64];
  buttonState = digitalRead(PC13);//Reading button pin
  
  if((isMaster == true) && (next == true)) {
    next = false;
    if(buttonState == 0){
      Message = Set_Lights_PWM();
      Serial.println(Message);
      Serial.println(String(BufferSize));
    }
    Serial.println(Message);
    Serial.println(String(BufferSize));
    int counter = 1;
    for(int i = 0; i < BufferSize; i++){

      if(counter%5 == 0 || i+1==BufferSize){
        loraRadio.write(PWM_Msg, sizeof(PWM_Msg));
        counter = 1;
        Serial.println("Part of the message: ");
        Serial.println((char*)PWM_Msg);
        delay(SEND_PERIOD_MS);
        PWM_Msg[counter-1] = (uint8_t)Message[i];
        counter++;
      }else{
        PWM_Msg[counter-1] = (uint8_t)Message[i];
        counter++;
      }
      
    }
    
    
    timer = millis();
  }

  if(isMaster && loraRadio.read(rcvData) > 0){
    Serial.println((char *)rcvData);
  }

  if(loraRadio.read(rcvData) > 0 && isMaster == false) {
    Serial.println("SOME DATA\n");
    Serial.println((char*)rcvData);
    //RecieveLoraData(rcvData);
    /*if(PWM_RX(rcvData)) {
      PWM_Ready = true;
      //loraRadio.write(OkMessage, 2);
    }*/
  }

  if(!isMaster && PWM_Ready){
    PWM_period(4,DutyCycle,Frequency);
  }

  if(((millis() - timer) >= SEND_PERIOD_MS) && (isMaster == true)){
    next = true;
  }
  
}
