#include <LoRaWan.h>
#include "keys.h"

// EU LoRa configuration
#define FREQ_RX_WNDW_SCND_EU  869.525
#define DOWNLINK_DATA_RATE_EU DR8
#define EU_RX_DR DR8 //EU max data rate for uplink channels = DR5
#define UPLINK_DATA_RATE_MAX_EU  DR5
#define MAX_EIRP_NDX_EU  2 //The min uplink data rate for all countries / plans is DR0
#define UPLINK_DATA_RATE_MIN DR0
#define DEFAULT_RESPONSE_TIMEOUT 5

// variables
const float EU_channels[8] = {868.1, 868.3, 868.5, 867.1, 867.3, 867.5, 867.7, 867.9}; //rx 869.525
unsigned char frame_counter = 1;
//buffer of 256 bits
char buffer[256];

void setup(void)
{
  
    //baud rate init and serial usb check
    SerialUSB.begin(115200);
    while(!SerialUSB);     

    // starts serialLora with baud rate 9600
    lora.init();
    // sends at+fdefault=risinghf & writes the outcome to serialUSB
    lora.setDeviceDefault();

    // fills buffer with 0s = buffer clear
    memset(buffer, 0, 256);

    //at+ver & fills buffer with output
    lora.getVersion(buffer, 256, 1);
    SerialUSB.print(buffer); 

    // buffer clear   
    memset(buffer, 0, 256);
    //(*buffer, length, timeout)
    lora.getId(buffer, 256, 1);   

    // communication authorization
    lora.setId(DEV_ADDR, DEV_EUI, APP_EUI);
    lora.setKey(NWK_S_KEY, APP_S_KEY, NULL);

    // sets ABP mode
    lora.setDeciveMode(LWABP);
    // sets EU data rate
    lora.setDataRate(DR0, EU868);
    //lora.setDataRate(DR0, US915HYBRID);
    // sends at+power=MAX_EIRP_NDX_EU
    lora.setPower(MAX_EIRP_NDX_EU);
    setChannelsFreq(EU_channels);
    //lora.setReceiceWindowFirst(0);
    //lora.setReceiceWindowSecond(FREQ_RX_WNDW_SCND_EU, DOWNLINK_DATA_RATE_EU);
}

// sets the channels frequencies
void setChannelsFreq(const float* channels){
    
    for(int i = 0; i < 8; i++){
        // DR0 is the min data rate
        // EU_RX_DR = DR5 is the max data rate for the EU
        if(channels[i] != 0){
          lora.setChannel(i, channels[i], UPLINK_DATA_RATE_MIN, UPLINK_DATA_RATE_MAX_EU);
        }
    }
}

void loop(void)
{   
    bool result = false;

    delay(2000);
    // uplink ack
    result = lora.transferPacket("UpAck!", 10);

    if(result)
    {
        short length;
        short rssi;

        memset(buffer, 0, 256);
        // downlink receive
        length = lora.receivePacket(buffer, 256, &rssi);

        // evaluating downlink data
        if(length)
        {
            SerialUSB.print("Length is: ");
            SerialUSB.println(length);
            SerialUSB.print("RSSI is: ");
            SerialUSB.println(rssi);
            SerialUSB.print("Data is: ");
            for(unsigned char i = 0; i < length; i ++)
            {
                //SerialUSB.print(buffer[i], HEX);
                SerialUSB.print(buffer[i]);
            }
            SerialUSB.println();
        }
    }
}
