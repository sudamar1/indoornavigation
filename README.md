# VLC based Indoor Navigation Project

The project's aim is to create a test environment for various VLC (Visual light communication) navigation algorithms used indoors. The test environment consists of LED equipped microcontroller devices (End nodes that communicates with the central unit (gateway) via the LORAWAN network. The gateway forms an intermediary between the end devices and the configuration application. The application allows you to reconfigure the test environment (e.g. change modulation type in the VLC channel, frequencies and duty cycle). The picture below presents the system architecture. 

>:warning: **See section [Quick start](#quick-start) for the quick environment setup.**

<div align="center">
    <img src="./docs/architecture/platform_arch.png" alt="Architecture">
</div>

## Repository structure

1. [docs](/docs)
    - All general documentation (e.g. technology docs...).
2. [structure](/structure)
    - Presents individual architectue parts:
      - [Gateway](./structure/gateway/),
      - [Node class C](./structure/nodes/classC/),
      - [Configuration app](./structure/app/jsApp/).
    - Each part contains its installation and use guides.


## Quick start

1. Start and connect gateway using SSH.
   1. Find gateway's ip adress
        ```
        ifconfig # windows
        ipconfig # linux
        ```
   2. Use Putty or terminal to controle the gateway:
       - Ethernet cable
            ```
            pi@<gw_ip>
            pswd: indoornavi
            ```
    3. Execute gwstart.sh in home directory.
        ```
        ./gwstart.sh
        # or
        source gwstart.sh
        ```
    4. Check status with checkStatus.sh.
        ```
        ./checkStatus.sh
        ```
    - All services should be active, else try resetPin.sh & run gwstart.sh again. 
    5. Check the gw backend in browser running at:
        ```
        http://<gw_ip>:8080/
        username: Marty
        pswd: indoornavi
        ```
2. Start Nodes by plugging them into an electrical outlet
   - Nodes will connect automatically to the LoRa network.
   1. Trace node through serial communication
      - Putty configuration:
        ```
        COM<num> # find in device manageer
        clk speed: 115200
        ```
3. Setup the configuration app on your machine with Docker. Please see the setup guide [here](./structure/app/jsApp/README.md).
    